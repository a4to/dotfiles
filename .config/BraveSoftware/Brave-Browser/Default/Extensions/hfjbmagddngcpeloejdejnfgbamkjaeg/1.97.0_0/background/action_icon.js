"use strict"
;__filename="background/action_icon.js",define(["require","exports","./store","./utils","./i18n","./browser","./ports"],function(n,e,t,i,a,u,o){
var r,l,c,s;Object.defineProperty(e,"__esModule",{value:true}),e.ei=e.ti=void 0,i=i,
r=["/icons/enabled.bin","/icons/partial.bin","/icons/disabled.bin"],e.ti=u.Bn.action||u.Bn.browserAction,c=function(n){
fetch(r[n]).then(function(n){return n.arrayBuffer()}).then(function(e){
var a,u,o,r=new Uint8ClampedArray(e),c=e.byteLength/5,s=0|Math.sqrt(c/4),f=s+s,b=i.fn()
;for(b[s]=new ImageData(r.subarray(0,c),s,s),b[f]=new ImageData(r.subarray(c),f,f),t.y[n]=b,a=l.get(n),l.delete(n),u=0,
o=a.length;u<o;u++)t.ie.has(a[u])&&t.C(a[u],n,true)})},e.ei=function(){var n,e,i=t.H;i!==!!t.y&&(t.C=i?s:t.S,
n=function(n){var e=n.Ln.s;0!==e.je&&t.C(e.Hn,i?e.je:0)},e=function(){return t.H===i},i?(t.y=[null,null,null],l=new Map,
o.po(n,e)):setTimeout(function(){t.H||null==t.y||(t.y=null,l=null)},200))},s=function(n,i,a){var o,r,s
;n<0||((o=t.y[i])?(r=e.ti.setIcon,s={tabId:n,imageData:o
},a?r(s,u.Mn):r(s)):l.has(i)?l.get(i).push(n):(setTimeout(c,0,i),l.set(i,[n])))},e.ei()});