"use strict"
;__filename="background/all_commands.js",define(["require","exports","./utils","./store","./browser","./normalize_urls","./parse_urls","./settings","./ports","./ui_css","./i18n","./key_mappings","./run_commands","./run_keys","./clipboard","./open_urls","./frame_commands","./filter_tabs","./tab_commands","./tools"],function(n,e,o,t,r,i,u,f,a,l,s,c,d,v,m,p,b,h,k,y){
Object.defineProperty(e,"__esModule",{value:true
}),o=o,f=f,t.I=[0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,2,0,1,0,0,0,0,2,0,1,0,2,2,0,0,1,0,0,1,0,0,1,0,2,1,1,0,0,0,0,0],
t.N=[function(){var n=t.z.for||t.z.wait
;"ready"!==n?(n=n?Math.abs("count"===n||"number"===n?t.T:0|n):d.hasFallbackOptions(t.z)?Math.abs(t.T):0)&&((n=Math.max(34,n))>17&&n<=1e3&&t.U&&t.U.postMessage({
N:16,t:n+50}),d.runNextCmdBy(t.T>0?1:0,t.z,n)):d.runNextOnTabLoaded({},null,function(){d.runNextCmdBy(1,t.z,1)})
},function(){
var n=t.z.rel,e=n?(n+"").toLowerCase():"next",o=null!=t.z.isNext?!!t.z.isNext:!e.includes("prev")&&!e.includes("before"),r=m.xr(t.z)
;m.doesNeedToSed(8192,r)?Promise.resolve(a.Rn(t.ie.get(t.U.s.Hn).Qn)).then(function(n){
var i=o?t.T:-t.T,u=n&&t.f(n,8192,r),f=u?p.goToNextUrl(u,i,!t.z.absolute||"absolute"):[false,n],a=f[1];f[0]&&a?(t.T=i,
null==t.z.reuse&&d.overrideOption("reuse",0),d.overrideCmdOptions({url_f:a,goNext:false}),
p.openUrl()):b.framesGoNext(o,e)}):b.framesGoNext(o,e)},function(){
var n,e=t.z.key,o=null!=(n=t.z.hideHUD)||null!=(n=t.z.hideHud)?!n:!t.ge.hideHud,r=e&&"string"==typeof e?c.dt(e).trim():""
;r=r.length>1||1===r.length&&!/[0-9a-z]/i.test(r)?r:"",
Promise.resolve(o?s.Xn("globalInsertMode",[r&&": "+(1===r.length?'" '.concat(r,' "'):"<".concat(r,">"))]):null).then(function(n){
d.sendFgCmd(7,o,{h:n,k:r||null,i:!!t.z.insert,p:!!t.z.passExitKey,r:+!!t.z.reset,t:d.parseFallbackOptions(t.z),
u:!!t.z.unhover})})},b.nextFrame,b.parentFrame,b.performFind,function(n){
var e=(t.z.key||"")+"",o="darkMode"===e?"d":"reduceMotion"===e?"m":f.eo[e],r=o?t.he[o]:0,i=s.Xn("quoteA",[e]),u=t.z.value,l="boolean"==typeof u,c=null,v=""
;o?"boolean"==typeof r?l||(u=null):l||void 0===u?c=l?"notBool":"needVal":typeof u!=typeof r&&(v=JSON.stringify(r),
c="unlikeVal",v=v.length>10?v.slice(0,9)+"\u2026":v):c=e in f.to?"notFgOpt":"unknownA",
Promise.resolve(i).then(function(e){var r,i,l,m;if(c)a.showHUD(s.Xn(c,[e,v]));else{for(l of(u=f.ao(o,u),
i=(r=t.ie.get(t.U.s.Hn)).Ln,r.wo))d.portSendFgCmd(l,8,m=l===i,{k:o,n:m?e:"",v:u},1);n(1)}})},function(){
0!==t.U.s.Cn||64&t.U.s.Kn?(new Promise(function(e,o){n([t.e.HelpDialogJS],e,o)}).then(n=>n),
d.sendFgCmd(17,true,t.z)):b.initHelp({a:t.z},t.U)},function(){var n,e,r,i,u,f,a,l,s,c,v=d.copyCmdOptions(o.fn(),t.z)
;if(!v.esc){
if(n=v.key,e=(v.type||(n?"keydown":""))+"",v.click)e="click";else if(t.T<0)for(r of"down up;enter leave;start end;over out".split(";"))i=r.split(" "),
e=e.replace(i[0],i[1]);for(a of(v.type=e,f=(u=v.init)&&("object"==typeof u?Object.assign({},u):v.init=null)||v,
["bubbles","cancelable","composed"]))f[a]=false!==f[a]||false!==v[a]
;n&&","!==n&&("object"==typeof n||n.includes(","))&&(l="object"==typeof n?n:n.split(",")).length>=2&&(!l[1]||+l[1]>=0)&&(c=0|l[1],
f.key="Space"===(s=l[0])?" ":"Comma"===s?",":"$"===s&&s.length>1?s=s.slice(1):s,c&&null==f.keyCode&&(f.keyCode=+l[1]),
c&&null==f.which&&(f.which=+l[1]),l.length>=3&&null==f.code&&(f.code=l[2]||l[0]))}d.portSendFgCmd(t.U,16,false,v,t.T)
},function(){b.showVomnibar()},b.enterVisualMode,function(n){var e=t.z.folder||t.z.path,o=!!t.z.all
;if(!e||"string"!=typeof e)return a.showHUD('Need "folder" to refer a bookmark folder.'),void n(0)
;t.findBookmark(1,e).then(function(e){
if(!e||null!=e.u)return n(0),void a.showHUD(false===e?'Need valid "folder".':null===e?"The bookmark folder is not found.":"The bookmark is not a folder.")
;(!o&&t.T*t.T<2?r.getCurTab:r.Fn)(function i(u){var f,l,s,c,v,m,p;if(!u||!u.length)return n(0),r.Mn()
;if(l=u[f=r.selectIndexFrom(u)],s=o?[0,u.length]:h.getTabRange(f,u.length),c=t.z.filter,v=u,u=u.slice(s[0],s[1]),
!c||(u=h.mu(l,u,c)).length)if((m=u.length)>20&&d.vu())d.cu("addBookmark",m).then(i.bind(0,v));else{
for(p of u)r.Bn.bookmarks.create({parentId:e.uo,title:p.title,url:r.getTabUrl(p)},r.Mn)
;a.showHUD("Added ".concat(m," bookmark").concat(m>1?"s":"",".")),n(1)}else n(0)})})},function(n){
false!==t.z.copied?(d.overrideCmdOptions({copied:true}),p.openUrl()):n(0)},b.captureTab,function(n){n(y.St.At(t.z,t.U))
},function(n){var e=t.U?t.U.s.Sn:2===t.ne;y.Ct.mr(e),Promise.resolve(e?s.Xn("incog"):"").then(function(e){
a.showHUD(s.Xn("fhCleared",[e])),n(1)})},function(n){var e=t.z.local?t.z.all?y.It.Et("#"):a.Nn({H:19,u:"",a:2
},true):y.It.Et();e&&e instanceof Promise?e.then(function(e){e&&n(1)}):n(1)},k.copyWindowInfo,function n(e,o,i){
var u,f,a=t.z.$pure;null==a&&d.overrideOption("$pure",a=!Object.keys(t.z).some(function(n){
return"opener"!==n&&"position"!==n&&"evenIncognito"!==n&&"$"!==n[0]
})),a?!(u=e&&e.length>0?e[0]:null)&&t.oe>=0&&!r.Mn()&&"dedup"!==i?r.jn(r.tabsGet,t.oe).then(function(e){
n(e&&[e],0,"dedup")}):(f=true===t.z.opener,r.openMultiTabs(u?{active:true,windowId:u.windowId,openerTabId:f?u.id:void 0,
index:p.newTabIndex(u,t.z.position,f,true)}:{active:true},t.T,t.z.evenIncognito,[null],true,u,function(n){
n&&r.selectWndIfNeed(n),d.getRunNextCmdBy(3)(n)})):p.openUrl(e)},function(n,e){
if(t.we<54)return a.showHUD(s.Xn("noDiscardIfOld",[54])),void e(0);h.xu(true,1,function n(e,o,i,u){
var f,l,c,v,m,p,b,k,y,M=o[0],w=o[1],g=o[2];if(u&&(M=(f=h.getTabRange(w,e.length,0,1))[0],g=f[1]),l=t.z.filter,c=e,
e=e.slice(M,g),
v=r.selectFrom(e),m=(e=l?h.mu(v,e,l):e).includes(v)?e.length-1:e.length)if(m>20&&d.vu())d.cu("discardTab",m).then(n.bind(null,c,[M,w,g],i));else{
for(y of(b=[],
(k=!(p=e[h.getNearArrIndex(e,v.index+(t.T>0?1:-1),t.T>0)]).discarded)&&(m<2||false!==p.autoDiscardable)&&b.push(r.jn(r.An.discard,p.id)),
e))y===v||y===p||y.discarded||(k=true,false!==y.autoDiscardable&&b.push(r.jn(r.An.discard,y.id)))
;b.length?Promise.all(b).then(function(n){var e=n.filter(function(n){return void 0!==n}),o=e.length>0
;a.showHUD(o?"Discarded ".concat(e.length," tab(s)."):s.Xn("discardFail")),i(o)
}):(a.showHUD(k?s.Xn("discardFail"):"Discarded."),i(0))}else i(0)},n,e)},function(n){var e,o=t.U?t.U.s.Hn:t.oe
;if(o<0)return a.complainLimits(s.Xn("dupTab")),void n(0);e=false===t.z.active,r.jn(r.An.duplicate,o).then(function(i){
if(i){if(e&&r.selectTab(o,r.Mn),e?n(1):d.runNextOnTabLoaded(t.z,i),!(t.T<2)){var u=function(n){r.openMultiTabs({
url:r.getTabUrl(n),active:false,windowId:n.windowId,pinned:n.pinned,index:n.index+2,openerTabId:n.id
},t.T-1,true,[null],true,n,null)};t.we>=52||0===t.ne||t.e.Ge?r.tabsGet(o,u):r.getCurWnd(true,function(e){
var i,f=e&&e.tabs.find(function(n){return n.id===o});if(!f||!e.incognito||f.incognito)return f?u(f):r.Mn()
;for(i=t.T;0<--i;)r.An.duplicate(o);n(1)})}}else n(0)}),e&&r.selectTab(o,r.Mn)},function(n){n.length&&b.framesGoBack({
s:t.T,o:t.z},null,n[0])},function(n){var e=!!t.z.absolute,o=t.z.filter,i=function(i){
var u,f,a,l,s,c,d=t.T,v=r.selectFrom(i),m=i.length;if(!o||(i=h.mu(v,i,o)).length){if(u=i.length,
f=h.getNearArrIndex(i,v.index,d<0),
a=(a=e?d>0?Math.min(u,d)-1:Math.max(0,u+d):Math.abs(d)>2*m?d>0?u-1:0:f+d)>=0?a%u:u+(a%u||-u),
i[0].pinned&&t.z.noPinned&&!v.pinned&&(d<0||e)){for(l=1;l<u&&i[l].pinned;)l++;if((u-=l)<1)return void n(0)
;e||Math.abs(d)>2*m?a=e?Math.max(l,a):a||l:(a=(a=f-l+d)>=0?a%u:u+(a%u||-u),a+=l)}
(c=!(s=i[a]).active)&&r.selectTab(s.id),n(c)}else n(0)},u=function(e){h.xu(true,1,i,e||[],n,null)}
;e?1!==t.T||o?u():r.jn(r.An.query,{windowId:t.te,index:0}).then(function(n){n&&n[0]&&r.qn(n[0])?i(n):u()
}):1===Math.abs(t.T)?r.jn(r.getCurTab).then(u):u()},function(){var n,e,o
;"frame"!==t.z.type&&t.U&&t.U.s.Cn&&(t.U=(null===(n=t.ie.get(t.U.s.Hn))||void 0===n?void 0:n.Qn)||t.U),e={H:3,u:"",
p:t.T,t:t.z.trailingSlash,r:t.z.trailing_slash,s:m.xr(t.z),e:false!==t.z.reloadOnRoot},o=a.Nn(e),
Promise.resolve(o||"url").then(function(){"object"==typeof e.e&&d.getRunNextCmdBy(2)(null!=e.e.p||void 0)})
},k.joinTabs,b.mainFrame,function(n,e){var o,i,u=r.selectIndexFrom(n)
;n.length>0&&(t.T<0?0===(t.T<-1?u:n[u].index):t.T>1&&u===n.length-1)?e(0):h.xu(true,1,function(o){
for(var u,f,a=r.selectIndexFrom(o),l=o[a],s=l.pinned,c=Math.max(0,Math.min(o.length-1,a+t.T));s!==o[c].pinned;)c-=t.T>0?1:-1
;if(c!==a&&i&&(u=r.getGroupId(l),
(f=r.getGroupId(o[c]))!==u&&(1===Math.abs(t.T)||u!==r.getGroupId(o[t.T>0?c<o.length-1?c+1:c:c&&c-1])))){
for(null!==u&&(a>0&&r.getGroupId(o[a-1])===u||a+1<o.length&&r.getGroupId(o[a+1])===u)&&(c=a,
f=u);0<=(c+=t.T>0?1:-1)&&c<o.length&&r.getGroupId(o[c])===f;);c-=t.T>0?1:-1}
c===a&&l.active?e(0):r.An.move((l.active?l:n[0]).id,{index:o[c].index},r.zn(e))
},n,e,(i="ignore"!==(o=t.z.group)&&false!==o)?function(e){return r.getGroupId(n[0])===r.getGroupId(e)}:null)
},k.moveTabToNewWindow,k.moveTabToNextWindow,function(){p.openUrl()},function(n,e){h.xu(!t.z.single,0,k.reloadTab,n,e)
},function(n,e){h.xu(false,1,function(n,e,o){r.An.remove(n[e[0]].id,r.zn(o))},n,e)},k.removeTab,function(n){
var e=t.z.other?0:t.T;h.gu(e,function o(i){var u,f,a,l,s,c=i;if(!c||0===c.length)return r.Mn();u=r.selectIndexFrom(c),
f=t.z.noPinned,
a=t.z.filter,l=c[u],e>0?(++u,c=c.slice(u,u+e)):(f=null!=f?f&&c[0].pinned:u>0&&c[0].pinned&&!c[u-1].pinned,
e<0?c=c.slice(Math.max(u+e,0),u):c.splice(u,1)),f&&(c=c.filter(function(n){return!n.pinned})),a&&(c=h.mu(l,c,a)),
(s=t.z.mayConfirm)&&c.length>("number"==typeof s?Math.max(s,5):20)&&d.vu()?d.cu("closeSomeOtherTabs",c.length).then(o.bind(null,i)):c.length>0?r.An.remove(c.map(function(n){
return n.id}),r.zn(n)):n(0)})},function(n,e){if(n.length<=0)e(0);else{var o=n[0],i=false!==t.z.group
;t.we>=52||0===t.ne||t.e.Ge||!r.xn(r.getTabUrl(o))?k.Ft(o,void 0,void 0,i):r.$n.get(o.windowId,function(n){
n.incognito&&!o.incognito&&(o.openerTabId=o.windowId=void 0),k.Ft(o,void 0,void 0,i)})}},function(n){
var e,o,i,u,f,l,c,v,m=r.Tn();if(!m)return n(0),a.complainNoSession();if(e=!!t.z.one,
o=Math.min(+m.MAX_SESSION_RESULTS||25,25),(i=Math.abs(t.T))>o){if(e)return n(0),void a.showHUD(s.Xn("indexOOR"));i=o}
if(!e&&i<2&&(t.U?t.U.s.Sn:2===t.ne)&&!t.z.incognito)return n(0),a.showHUD(s.Xn("notRestoreIfIncog"))
;if(u=false===t.z.active,f=t.U?t.U.s.Hn:t.oe,l=d.getRunNextCmdBy(0),c=u?function(){r.Mn()?n(0):r.selectTab(f,l)}:l,
e&&i>1)m.getRecentlyClosed({maxResults:i},function(e){if(!e||i>e.length)return n(0),a.showHUD(s.Xn("indexOOR"))
;var o=e[i-1],t=o&&(o.tab||o.window);t?m.restore(t.sessionId,c):n(0)});else if(1===i)m.restore(null,c);else{
for(v=[];0<=--i;)v.push(r.jn(m.restore,null));Promise.all(v).then(function(e){void 0===e[0]?n(0):u?r.selectTab(f,l):n(1)
})}u&&r.selectTab(f,r.Mn)},function(){null==t.z.$seq?v.runKeyWithCond():v.runKeyInSeq(t.z.$seq,t.T,t.z.$f,null)
},function(n){var e,o,f,l=(t.z.keyword||"")+"",c=u.fu({u:r.getTabUrl(n[0])});c&&l?(e=m.xr(t.z),c.u=t.f(c.u,0,e),
o=i.Qe(c.u.split(" "),l,2),d.overrideCmdOptions({url_f:o,reuse:null!=(f=t.z.reuse)?f:0,opener:true,keyword:""}),
p.openUrl(n)):d.runNextCmd(0)||a.showHUD(s.Xn(l?"noQueryFound":"noKw"))},function(n){var e,o=t.z.id,i=t.z.data
;o&&"string"==typeof o&&void 0!==i?(e=Date.now(),r.Bn.runtime.sendMessage(o,t.z.raw?i:{handler:"message",
from:"Vimium C",count:t.T,keyCode:t.F,data:i},function(t){var i=r.Mn()
;return i?(console.log("Can not send message to the extension %o:",o,i),a.showHUD("Error: "+(i.message||i)),
n(0)):"string"==typeof t&&Math.abs(Date.now()-e)<1e3&&a.showHUD(t),i||n(false!==t),i
})):(a.showHUD('Require a string "id" and message "data"'),n(0))},function(n){var e,o=t.z.text
;o||!t.z.$f||(o=(e=t.z.$f)&&e.t?s.br("".concat(e.t)):"")?(a.showHUD(o?o instanceof Promise?o:o+"":s.Xn("needText")),
n(!!o)):n(false)},function(n,e){y.St.Pt(t.z,t.T,n,e)},k.toggleMuteTab,function(n,e){h.xu(true,0,k.togglePinTab,n,e)
},k.toggleTabUrl,function(n,e){var o,r=n[0].id,i=((t.z.style||"")+"").trim(),u=!!t.z.current
;if(!i)return a.showHUD(s.Xn("noStyleName")),void e(0);for(o of t.ue)if(o.s.Hn===r)return o.postMessage({N:46,t:i,c:u}),
void setTimeout(e,100,1);u||l.ii({t:i,o:1}),setTimeout(e,100,1)},b.toggleZoom,function(n){
var e,o=!!t.z.acrossWindows,i=!!t.z.onlyActive,u=t.z.filter,f={},l=function(e){var o,f,l,c,d
;if(e.length<2)return i&&a.showHUD("Only found one browser window"),n(0),r.Mn();o=t.U?t.U.s.Hn:t.oe,
l=(f=e.findIndex(function(n){return n.id===o}))>=0?e[f]:null,f>=0&&e.splice(f,1),
!u||(e=h.mu(l,e,u)).length?(c=e.filter(function(n){return t.ee.has(n.id)}).sort(y._t.Lr),
(d=(e=i&&0===c.length?e.sort(function(n,e){return e.id-n.id
}):c)[t.T>0?Math.min(t.T,e.length)-1:Math.max(0,e.length+t.T)])?i?r.$n.update(d.windowId,{focused:true
},r.zn(n)):s(d.id):n(0)):n(0)},s=function(e){r.selectTab(e,function(e){return e&&r.selectWndIfNeed(e),r.zn(n)()})}
;1===t.T&&!i&&-1!==t.oe&&(e=h.wu())>=0?Promise.all([r.jn(r.tabsGet,e),h.getNecessaryCurTabInfo(u)]).then(function(n){
var e=n[0],i=n[1];e&&(o||e.windowId===t.te)&&r.qn(e)&&(!u||h.mu(i,[e],u).length>0)?s(e.id):o?r.An.query(f,l):r.Fn(l)
}):o||i?r.An.query(i?{active:true}:f,l):r.Fn(l)},function(n){var e=t.z.newWindow
;true!==e&&true?r.jn(r.Bn.permissions.contains,{permissions:["downloads.shelf","downloads"]}).then(function(o){var i,u
;if(o){i=r.Bn.downloads.setShelfEnabled,u=void 0;try{i(false),setTimeout(function(){i(true),n(1)},256)}catch(n){
u=(n&&n.message||n)+""}a.showHUD(u?"Can not close the shelf: "+u:"The download bar has been closed"),u&&n(0)
}else false===e&&t.U?(a.showHUD("No permissions to close download bar"),n(0)):t.N[27](n)}):t.N[27](n)},function(n){
var e,o=t.ie.get(t.U?t.U.s.Hn:t.oe);for(e of o?o.wo:[])d.portSendFgCmd(e,7,false,{r:1},1);o&&o.Ln.postMessage({N:16,
t:150}),n(1)},function(n){var e,o,r,i,u,f,l,s=t.z.$cache;if(null!=s&&((o=t.se.ve.find(function(n){return n.uo===s
}))?e=Promise.resolve(o):d.overrideOption("$cache",null)),r=!!e,i=t.T,u=false,!e){
if(!(f=t.z.path||t.z.title)||"string"!=typeof f)return a.showHUD("Invalid bookmark "+(t.z.path?"path":"title")),
void n(0)
;if(!(l=d.fillOptionWithMask(f,t.z.mask,"name",["path","title","mask","name","value"],i)).ok)return void a.showHUD((l.result?"Too many potential names":"No name")+" to find bookmarks")
;u=l.useCount,e=t.findBookmark(0,l.result)}e.then(function(e){e&&null!=e.u?(r||u||d.overrideOption("$cache",e.uo),
d.overrideCmdOptions({url:e.fo||e.u
},true),t.T=u?1:i,p.openUrl()):(n(0),a.showHUD(false===e?'Need valid "title" or "title".':null===e?"The bookmark node is not found.":"The bookmark is a folder."))
})}]});