"use strict";__filename="background/browser.js",define(["require","exports","./store","./utils"],function(n,e,r,u){
var t,o,i,f,l,c,a,s,d,v,m,p,b,g;Object.defineProperty(e,"__esModule",{value:true}),
e.import2=e._n=e.In=e.xn=e.removeTempTab=e.downloadFile=e.makeTempWindow_r=e.makeWindow=e.openMultiTabs=e.tabsCreate=e.selectWndIfNeed=e.selectWnd=e.selectTab=e.Pn=e.zn=e.jn=e.Wn=e.selectIndexFrom=e.selectFrom=e.getCurWnd=e.qn=e.Fn=e.getCurTabs=e.getCurTab=e.isTabMuted=e.getTabUrl=e.getGroupId=e.tabsUpdate=e.tabsGet=e.Mn=e.On=e.Tn=e.$n=e.An=e.Bn=void 0,
e.Bn=chrome,e.An=e.Bn.tabs,e.$n=e.Bn.windows,e.Tn=function(){return e.Bn.sessions},e.On=function(){
return e.Bn.webNavigation},e.Mn=function(){return e.Bn.runtime.lastError},e.tabsGet=e.An.get,e.tabsUpdate=e.An.update,
e.getGroupId=function(n){var e=n.groupId;return-1!==e&&null!=e?e:null},e.getTabUrl=function(n){
return n.url||n.pendingUrl},e.isTabMuted=function(n){return n.mutedInfo.muted},e.getCurTab=e.An.query.bind(null,{
active:true,currentWindow:true}),e.getCurTabs=e.An.query.bind(null,{currentWindow:true}),e.Fn=e.getCurTabs,
e.qn=function(){return true},e.getCurWnd=function(n,u){var t={populate:n}
;return r.te>=0?e.$n.get(r.te,t,u):e.$n.getCurrent(t,u)},t=function(n){return n[e.selectIndexFrom(n)]},e.selectFrom=t,
e.selectIndexFrom=function(n){for(var e=n.length;0<--e;)if(n[e].active)return e;return 0},e.Wn=function(n){
return/^(edge-)?extension:/.test(n)?r.e.xe+"-"+n.slice(n.indexOf("ext")):n},e.jn=function(n){
var r=[].slice.call(arguments,1),t=u.l(),o=t.kn,i=t.yn;return r.push(function(n){var r=e.Mn()
;return i(r?void 0:null!=n?n:null),r}),n.apply(void 0,r),o},o=function(n){return n!==r.S?function(){var r=e.Mn()
;return n(!r),r}:e.Mn},e.zn=o,e.Pn=function(n){return new Promise(function(e){n(e)})},i=function(n,e){var u=r.me.get(n)
;return 1===u||2===u&&!(!r.be&&!e)},f=function(n,r){e.tabsUpdate(n,{active:true},r)},e.selectTab=f,l=function(n){
return n&&e.$n.update(n.windowId,{focused:true}),e.Mn()},e.selectWnd=l,c=function(n){n.windowId!==r.te&&e.selectWnd(n)},
e.selectWndIfNeed=c,a=function(n,u,t){var o=n.url;return o?i(o,2===r.ne)&&delete n.url:(o=r.newTabUrl_f,
2===r.ne&&(-1===t?o.includes("pages/blank.html")&&o.startsWith(location.origin+"/"):!t&&o.startsWith(location.protocol))||i(o,2===r.ne)||(n.url=o),
n.url||delete n.url),e.An.create(n,u)},e.tabsCreate=a,s=function(n,r,u,t,o,i,f){var l;o=false!==o,
l=null!=i?e.getGroupId(i):null,o||null==l||delete n.index,l=o&&null!=l&&e.An.group?l:void 0,e.tabsCreate(n,function(u){
var o,i,c,a,s;if(e.Mn())return f&&f(),e.Mn();for(n.index=u.index,n.windowId=u.windowId,null!=l&&e.An.group({tabIds:u.id,
groupId:l}),f&&f(u),n.active=false,o=null!=n.index,i=t?t.length:1,c=null!=l?function(n){return n&&e.An.group({
tabIds:n.id,groupId:l}),e.Mn()
}:e.Mn,t.length>1&&(t[0]=n.url),a=0;a<r;a++)for(s=a>0?0:1;s<i;s++)t.length>1&&(n.url=t[s]),o&&++n.index,e.An.create(n,c)
},u)},e.openMultiTabs=s,d=function(n,u,t){var o,f=false!==n.focused
;(u=u?"minimized"===u===f||"popup"===n.type||"normal"===u||"docked"===u?"":u:"")&&!u.includes("fullscreen")&&(n.state=u,
u=""),
n.focused=true,(o=n.url)||null!=n.tabId||(o=n.url=r.newTabUrl_f),"string"==typeof o&&i(o,n.incognito)&&delete n.url,
e.$n.create(n,u||!f||t?function(n){if(t&&t(n),!(u||!f)||!n)return e.Mn();var r=f?{}:{focused:false};u&&(r.state=u),
e.$n.update(n.id,r)}:e.Mn)},e.makeWindow=d,v=function(n,r,u){var t="number"==typeof n;e.$n.create({type:"normal",
focused:false,incognito:r,state:"minimized",tabId:t?n:void 0,url:t?void 0:n},u)},e.makeTempWindow_r=v,
m=function(n,r,t,o){e.jn(e.Bn.permissions.contains,{permissions:["downloads"]}).then(function(t){var i,f,l,c;t?(i={url:n
},
r&&(f=/\.[a-z\d]{1,4}(?=$|[?&])/i,r=(r="#"===(r=u.on(r))[0]?r.slice(1):r).replace(/[\r\n]+/g," ").replace(/[/\\?%*:|"<>_]+/g,"_"),
f.test(r)||(l=f.exec(n),r+=l?l[0]:n.includes(".")?"":".bin"),i.filename=r),c=e.jn(e.Bn.downloads.download,i),
o&&c.then(function(n){return o(void 0!==n)})):o&&o(false)})},e.downloadFile=m,p=function(n){e.An.remove(n,e.Mn)},
e.removeTempTab=p,e.xn=function(n){
return n=n.slice(0,99).toLowerCase(),1!==r.me.get(n)&&(n.startsWith("about:")?"about:blank"!==n:n.startsWith("chrome:")?!n.startsWith("chrome://downloads"):n.startsWith(r.e.xe)&&!("string"!=typeof r.e.De?r.e.De.test(n):n.startsWith(r.e.De))||r.be&&/^(edge|extension):/.test(n)&&!n.startsWith("edge://downloads"))
},b=function(n,r){var u,t,o=e.Bn.permissions;u=Promise.all(n.map(function(n){return n&&e.jn(e.Bn.permissions.contains,n)
})),t=n.map(function(n){return n&&(n.permissions||n.origins)[0]}),u.then(function(n){
var e=false,u=false,i=function(i,c){var a,s,d,v,m=!c;if(c){for(s of(a=c.permissions)||[])(d=t.indexOf(s))>=0&&(n[d]=i,
m=true);for(v of(!a||a.length<=0)&&c.origins||[])if("chrome://*/*"!==v)(d=t.indexOf(v))>=0&&(n[d]=i,
m=true);else for(d=0;d<t.length;d++)(t[d]||"").startsWith("chrome://")&&(n[d]=i,m=true)}
m&&(false===r(n,true)&&(e=u=false),e!==n.includes(false)&&o.onAdded[(e=!e)?"addListener":"removeListener"](f),
u!==n.includes(true)&&o.onRemoved[(u=!u)?"addListener":"removeListener"](l))},f=i.bind(null,true),l=i.bind(null,false)
;n.includes(false)||n.includes(true)?i(true):r(n,false)})},e.In=b,g=function(n){var u,t=location.origin.length
;for(u of r.e.He.slice(0,-1))e.An.executeScript(n,{file:u.slice(t),allFrames:true},e.Mn)},e._n=g;e.import2=function(e){
return new Promise(function(r,u){n([e],r,u)}).then(n=>n)},r.D<6&&(r.V=new Promise(function(n){
var r=e.Bn.runtime.onInstalled,u=function(e){var t=u;t&&(u=null,n(e),r.removeListener(t))};r.addListener(u),
setTimeout(u,6e3,null)}))});