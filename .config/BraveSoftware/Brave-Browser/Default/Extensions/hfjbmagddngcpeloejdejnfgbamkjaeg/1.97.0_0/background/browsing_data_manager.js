"use strict"
;__filename="background/browsing_data_manager.js",define(["require","exports","./store","./browser","./utils","./settings","./completion_utils"],function(t,e,n,r,i,o,u){
var f,a,l,c,s,_,m,v,d,p,T,h;Object.defineProperty(e,"__esModule",{value:true
}),e.Pr=e.Ur=e.Br=e.Vr=e.Wr=e.qr=e.Hr=e.omniBlockList=void 0,i=i,o=o,f=decodeURIComponent,c=-1,s="1",_=null,v=null,
e.omniBlockList=m=null,e.Hr=function(t){var e,n,r=t.slice(0,5);if("https"===r)e=8;else if("http:"===r)e=7;else{
if(!r.startsWith("ftp"))return null;e=6}return n=t.indexOf("/",e),{
Gr:"__proto__"!==(t=t.slice(e,n<0?t.length:n))?t:".__proto__",Jr:e}},e.qr={Kr:null,Qr:"",Xr:0,Cr:0,Yr:null,
oi:function(){var t=r.Bn.bookmarks;t.onCreated.addListener(e.qr.ui),t.onRemoved.addListener(e.qr.fi),
t.onChanged.addListener(e.qr.fi),t.onMoved.addListener(e.qr.ui),t.onImportBegan.addListener(function(){
r.Bn.bookmarks.onCreated.removeListener(e.qr.ui)}),t.onImportEnded.addListener(function(){
r.Bn.bookmarks.onCreated.addListener(e.qr.ui),e.qr.ui()})},ai:function(){n.se.je=1,e.qr.Cr&&(clearTimeout(e.qr.Cr),
e.qr.Cr=0),r.Bn.bookmarks.getTree(e.qr.li)},li:function(t){n.se.ve=[],n.se.Ce=[],n.se.je=2,u.ci.Et(2),
t.forEach(e.qr.si,e.qr),setTimeout(function(){return e.Pr._i(n.se.ve)},50),e.qr.oi&&(setTimeout(e.qr.oi,0),e.qr.oi=null)
;var r=e.qr.Yr;e.qr.Yr=null,r&&r()},si:function(t){var r,o,u,f=t.title,a=t.id,l=f||a,c=e.qr.Qr+"/"+l
;t.children?(n.se.Ce.push({uo:a,mi:c,vi:l}),r=e.qr.Qr,2<++e.qr.Xr&&(e.qr.Qr=c),t.children.forEach(e.qr.si,e.qr),
--e.qr.Xr,e.qr.Qr=r):(u=(o=t.url).startsWith("javascript:"),n.se.ve.push({uo:a,mi:c,vi:l,t:u?"javascript:":o,
di:m?e.Br(o,f):1,u:u?"javascript:":o,fo:u?o:null,pi:u?i.on(o):null}))},Ti:function(){var t=performance.now()-n.se.Se
;0===n.se.je&&(t>=6e4||t<-5e3?(e.qr.Cr=n.se.Se=0,n.se.ke=false,e.qr.ai()):(n.se.ve=[],n.se.Ce=[],
e.qr.Cr=setTimeout(e.qr.Ti,3e4),u.ci.Et(2)))},ui:function(){n.se.Se=performance.now(),
n.se.je<2||(e.qr.Cr=setTimeout(e.qr.Ti,6e4),n.se.je=0)},fi:function(t,r){
for(var i,o,u,f,l,c,s=n.se.ve,_=s.length,v=r&&r.title,d=0;d<_&&s[d].uo!==t;d++);if(d<_)return o=(i=s[d]).u,u=r&&r.url,
a&&(null==v?o!==i.t||!r:null!=u&&o!==u)&&n.re.has(o)&&e.Wr.hi&&e.Wr.gi(o)<0&&n.re.delete(o),
void(null!=v?(i.mi=i.mi.slice(0,-i.vi.length)+(v||i.uo),i.vi=v||i.uo,u&&(i.u=u,i.t=e.Pr.bi(u,i),e.Pr.xi()),
m&&(i.di=e.Br(i.u,i.vi))):(s.splice(d,1),r||e.qr.ui()));if(n.se.Ce.find(function(e){return e.uo===t})){
if(null!=v)return e.qr.ui();if(!n.se.ke&&a){for(c of(f=n.re,l=e.Wr.gi,e.Wr.hi?s:[]))f.has(o=c.u)&&l(o)<0&&f.delete(o)
;n.se.ke=true}return e.qr.ui()}}},n.findBookmark=function(t,r){var o,u,f,a,l,c,s;if(2!==n.se.je)return o=i.l(),
e.qr.Yr=o.yn,e.qr.ai(),o.kn.then(n.findBookmark.bind(0,t,r))
;if(f=(u=r.includes("/"))?(r+"").replace(/\\\/?|\//g,function(t){return t.length>1?"/":"\n"
}).split("\n").filter(function(t){return t}):[],!r||u&&!f.length)return Promise.resolve(false)
;for(c of(a=u?"/"+f.slice(1).join("/"):"",
l=u?"/"+f[0]+a:"",t?[]:n.se.ve))if(u&&(c.mi===l||c.mi===a)||c.vi===r)return Promise.resolve(c)
;for(c of t?n.se.Ce:[])if(u&&(c.mi===l||c.mi===a)||c.vi===r)return Promise.resolve(c);for(c of(s=null,
t?[]:n.se.ve))if(c.vi.includes(r)){if(s){s=null;break}s=c}return Promise.resolve(s)},d=function(t){t&&t()},e.Wr={
hi:false,ki:0,wi:null,yi:function(t){e.Wr.wi?t&&e.Wr.wi.push(t):(n.ae.Ie=Date.now(),e.Wr.wi=t?[t]:[],
e.Wr.ki||r.Bn.history.search({text:"",maxResults:2e4,startTime:0},function(t){setTimeout(e.Wr.Di,0,t)}))},
Di:function(t){var i,o,u,f,a;for(e.Wr.Di=null,i=0,o=t.length;i<o;i++)(f=(u=t[i]).url).length>2e3&&(f=e.Wr.Li(f,u)),
t[i]={t:f,vi:u.title,Ri:u.lastVisitTime,di:1,u:f};if(m)for(a of t)0===e.Br(a.t,a.vi)&&(a.di=0);setTimeout(function(){
setTimeout(function(){var t,r,i,o,u,f,a,l,c=n.ae.Pe
;for(t=c.length-1;0<t;)for(u=(o=(r=c[t]).t=e.Pr.bi(i=r.u,r)).length>=i.length;0<=--t&&!((a=(f=c[t]).u).length>=i.length)&&i.startsWith(a);)f.u=i.slice(0,a.length),
l=u?a:e.Pr.bi(a,f),f.t=u||l.length<a.length?o.slice(0,l.length):l;e.Wr.ji&&setTimeout(function(){
e.Wr.ji&&e.Wr.ji(n.ae.Pe)},200)},100),n.ae.Pe.sort(function(t,e){return t.u>e.u?1:-1}),e.Wr.hi=true,
r.Bn.history.onVisitRemoved.addListener(e.Wr.Ii),r.Bn.history.onVisited.addListener(e.Wr.Mi)},100),n.ae.Pe=t,e.Wr.yi=d,
e.Wr.wi&&e.Wr.wi.length>0&&setTimeout(function(t){for(var e of t)e()},1,e.Wr.wi),e.Wr.wi=null},Mi:function(t){
var r,i,o,f,a,l,c,s,_,v=t.url;if(v.length>2e3&&(v=e.Wr.Li(v,t)),r=t.lastVisitTime,i=t.title,o=++n.ae.Ne,f=n.ae.Ve,
(a=e.Wr.gi(v))<0&&n.ae.Re++,(o>59||o>10&&Date.now()-n.ae.Ie>3e5)&&e.Wr.Pi(),l=a>=0?n.ae.Pe[a]:{t:"",vi:i,Ri:r,
di:m?e.Br(v,i):1,u:v},f&&(s=e.Hr(v))&&((c=f.get(s.Gr))?(c.Ri=r,a<0&&(c.Ui+=l.di),
s.Jr>6&&(c.lr=8===s.Jr?1:0)):f.set(s.Gr,{Ri:r,Ui:l.di,lr:8===s.Jr?1:0})),a>=0)return l.Ri=r,void(i&&i!==l.vi&&(l.vi=i,
u.ci.Xt&&u.ci.Et(1),m&&(_=e.Br(v,i),l.di!==_&&(l.di=_,c&&(c.Ui+=_||-1)))));l.t=e.Pr.bi(v,l),n.ae.Pe.splice(~a,0,l),
u.ci.Xt&&u.ci.Et(1)},Ii:function(t){var r,i,o,f,a,c,s,_,m;if(l.length=0,r=n.re,u.ci.Et(1),
t.allHistory)return n.ae.Pe=[],n.ae.Ve=new Map,i=new Set(n.se.ve.map(function(t){return t.u})),
void r.forEach(function(t,e){i.has(e)||r.delete(e)});for(s of(o=e.Wr.gi,f=n.ae.Pe,a=n.ae.Ve,
t.urls))(_=o(s))>=0&&(a&&f[_].di&&(m=e.Hr(s))&&(c=a.get(m.Gr))&&--c.Ui<=0&&a.delete(m.Gr),f.splice(_,1),r.delete(s))},
Li:function(t,e){var n=t.lastIndexOf(":",9),r=n>0&&"://"===t.substr(n,3),o=e.title
;return t=t.slice(0,(r?t.indexOf("/",n+4):n)+320)+"\u2026",o&&o.length>160&&(e.title=i.pn(o,0,160)),t},Pi:function(){
var t=Date.now();if(n.ae.Re<=0);else{if(t<n.ae.Ie+1e3&&t>=n.ae.Ie)return;setTimeout(r.Bn.history.search,50,{text:"",
maxResults:Math.min(999,n.ae.Ne+10),startTime:t<n.ae.Ie?t-3e5:n.ae.Ie},e.Wr.Ei)}return n.ae.Ie=t,n.ae.Re=n.ae.Ne=0,
e.Pr.xi()},ji:function(t){var r,i,o,u,f,a,l,c;for(i of(e.Wr.ji=null,r=n.ae.Ve,t))o=i.Ri,u=i.di,(f=e.Hr(i.u))&&(l=f.Jr,
(c=r.get(a=f.Gr))?(c.Ri<o&&(c.Ri=o),c.Ui+=u,l>6&&(c.lr=8===l?1:0)):r.set(a,{Ri:o,Ui:u,lr:8===l?1:0}))},Ei:function(t){
var r,i,o,u,f=n.ae.Pe,a=e.Wr.gi;if(!(f.length<=0))for(r of t){if((i=r.url).length>2e3&&(r.url=i=e.Wr.Li(i,r)),
(o=a(i))<0)n.ae.Re--;else if(!(u=r.title)||u===f[o].vi)continue;n.ae.Ne--,e.Wr.Mi(r)}},gi:function(t){
for(var e="",r=n.ae.Pe,i=r.length-1,o=0,u=0;o<=i;)if((e=r[u=o+i>>>1].u)>t)i=u-1;else{if(e===t)return u;o=u+1}return~o}},
p=function(t,n,i){var o=r.Tn();o?o.getRecentlyClosed({
maxResults:Math.min(Math.round(1.2*t),+o.MAX_SESSION_RESULTS||25,25)},function(t){var o,u,f,a,l,c,s;for(a of(o=[],f=0,
t||[]))(l=a.tab)?((c=l.url).length>2e3&&(c=e.Wr.Li(c,l)),s=l.title,(n||e.Br(c,s))&&o.push({u:c,vi:s,
Oi:(u=a.lastModified,u<3e11&&u>-4e10?1e3*u:u),Ai:[l.windowId,l.sessionId]})):f=1;return f?setTimeout(i,0,o):i(o),r.Mn()
}):i([])},e.Vr=p,e.Br=function(t,e){return v.test(e)||v.test(t)?0:1},e.Ur={Si:function(t){var e,n
;if(m)for(e of t)for(n of m)if(n=n.trim(),e.includes(n)||n.length>9&&e.length+2>=n.length&&n.includes(e))return true
;return false},Bi:function(){var t,r,i,o,u;if(n.se.ve)for(t of n.se.ve)t.di=m?e.Br(t.t,t.mi):1
;if(n.ae.Pe)for(t of(r=n.ae.Ve,
n.ae.Pe))i=m?e.Br(t.t,t.vi):1,t.di!==i&&(t.di=i,r&&(o=e.Hr(t.u))&&(u=r.get(o.Gr))&&(u.Ui+=i||-1))}},e.Pr={
bi:function(t,e){if(t.length>=400||t.lastIndexOf("%")<0)return t;try{return f(t)}catch(t){}
return n.re.get(t)||(e&&l.push(e),t)},_i:function(t){for(var r,i,o=n.re,u=l,a=-1,c=t.length;;)try{
for(;++a<c;)(r=t[a]).t=(i=r.u).length>=400||i.lastIndexOf("%")<0?i:f(i);break}catch(t){r.t=o.get(i)||(u.push(r),i)}
e.Pr.xi()},xi:function(){0!==l.length&&-1===c&&(c=0,setTimeout(T,17))}},T=function(){var t,e,r,i,o=l.length
;if(!s||c>=o)return l.length=0,c=-1,void(_=null)
;for(o=Math.min(c+32,o),_=_||new TextDecoder(s);c<o;c++)(t=n.re.get(i=(r="string"==typeof(e=l[c]))?e:e.u))?r||(e.t=t):(t=(t=i.replace(/%[a-f\d]{2}(?:%[a-f\d]{2})+/gi,h)).length!==i.length?t:i,
"string"!=typeof e?n.re.set(e.u,e.t=t):n.re.set(e,t));c<l.length?setTimeout(T,4):(l.length=0,c=-1,_=null)},
h=function(t){var e,n,r=new Uint8Array(t.length/3);for(e=1,n=0;e<t.length;e+=3)r[n++]=parseInt(t.substr(e,2),16)
;return _.decode(r)},n.ce.omniBlockList=function(t){var r,o=[];for(r of t.split("\n"))r.trim()&&"#"!==r[0]&&o.push(r)
;v=o.length>0?new RegExp(o.map(i.t).join("|"),""):null,e.omniBlockList=m=o.length>0?o:null,
(n.ae.Pe||n.se.ve.length)&&setTimeout(e.Ur.Bi,100)},o.lo.then(function(){o.co("omniBlockList")}),
n.ce.localeEncoding=function(t){var r=!!t&&!(t=t.toLowerCase()).startsWith("utf"),i=s;if((s=r?t:"")!==i){try{
new TextDecoder(s)}catch(t){r=false}r?"1"!==i&&setTimeout(function(){return n.ae.Pe&&e.Pr._i(n.ae.Pe),e.Pr._i(n.se.ve)
},100):(n.re.clear(),l&&(l.length=0)),a!==r&&(l=r?[]:{length:0,push:n.S},a=r,c=-1)}},o.co("localeEncoding"),
n.j._u=function(t,n,i){switch(n){case"tab":u.ci.Vi(null),r.An.remove(+t,function(){var t=r.Mn();return t||u.ci.Vi(null),
i(!t),t});break;case"history":var o=!e.Wr.hi||e.Wr.gi(t)>=0;r.Bn.history.deleteUrl({url:t}),o&&u.ci.Et(1),i(o)}},
n.j.Wi=e.Ur.Si});