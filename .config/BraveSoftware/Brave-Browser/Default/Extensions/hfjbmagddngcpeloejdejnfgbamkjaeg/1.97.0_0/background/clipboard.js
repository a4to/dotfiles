"use strict"
;__filename="background/clipboard.js",define(["require","exports","./store","./utils","./exclusions"],function(e,n,r,u,t){
var a,o,f,l,c,i,s,p,d,b,g,x;Object.defineProperty(n,"__esModule",{value:true}),n.doesNeedToSed=n.xr=void 0,u=u,t=t,a={
__proto__:null,atob:8,base64:8,base64decode:8,btoa:9,base64encode:9,decodeforcopy:1,decode:1,decodeuri:1,decodeurl:1,
decodemaybeescaped:2,decodeall:19,decodecomp:2,encode:10,encodecomp:11,encodeall:12,encodeallcomp:13,unescape:3,upper:4,
lower:5,capitalize:16,capitalizeall:17,camel:14,camelcase:14,dash:15,dashed:15,hyphen:15,normalize:6,reverse:7,
reversetext:7,break:99,stop:99,return:99,latin:18,latinize:18,latinise:18,noaccent:18,nodiacritic:18},o=null,
f=function(e,n){var r,t,o,f,c,i,p,d,b,g,x,_,v,m,h,w=[],y=new Map;for(r of e.split("\n"))if(r=r.trim(),
(t=/^([\w\x80-\ufffd]{1,6})([^\x00- \w\\\x7f-\uffff])/.exec(r))&&((f=y.get(o=t[2]))||(c="\\u"+(o.charCodeAt(0)+65536).toString(16).slice(1),
f=new RegExp("^((?:\\\\".concat(c,"|[^").concat(c,"])+)").concat(c,"(.*)").concat(c,"([a-z]{0,9})((?:,[A-Za-z-]+|,host=[\\w.*-]+)*)$")),
y.set(o,f)),i=f.exec(r.slice(t[0].length)))){for(_ of(p=t[1],d=i[3],b=[],g=null,x=0,
i[4].split(",")))(v=_.toLowerCase()).startsWith("host=")?g=_.slice(5):v.startsWith("match")?x=1:(m=a[v.replace(/-/g,"")]||0)&&b.push(m)
;(h=u.d(i[1],x?d.replace("g",""):d))&&w.push({zr:n||s(p),Rr:g,Ar:h,Er:x,Fr:b,Nr:l(i[2],1)})}return w},l=function(e,n){
return e.replace(/\\(x[0-9a-fA-F]{2}|u[0-9a-fA-F]{4}|[^])|\$0/g,function(e,r){
return r?"x"===r[0]||"u"===r[0]?"$"===(r=String.fromCharCode(parseInt(r.slice(1),16)))?r+r:r:"t"===r?"\t":"r"===r?"\r":"n"===r?"\n":n?"0"===r?"$&":r>="1"&&r<="9"?"$"+r:r:r:n?"$&":e
})},c=function(e,n){
var u=14===n,t=15===n,a=16===n,o=17===n,f=r.we<64||false?u||t?/(?:[-_\s\/+\u2010-\u2015]|(\d)|^)([a-z\u03b1-\u03c9]|[A-Z\u0391-\u03a9]+[a-z\u03b1-\u03c9]?)|[\t\r\n\/+]/g:a?/(\b|_)[a-z\u03b1-\u03c9]/:o?/(\b|_)[a-z\u03b1-\u03c9]/g:null:new RegExp(u||t?"(?:[-_\\t\\r\\n/+\\u2010-\\u2015\\p{Z}]|(\\p{N})|^)(\\p{Ll}|\\p{Lu}+\\p{Ll}?)|[\\t\\r\\n/+]":a||o?"(\\b|_)\\p{Ll}":"",a?"u":"ug"),l=0,c=0,i=function(e,n){
return n?e.toLocaleLowerCase():e.toLocaleUpperCase()};return e=u||t?e.replace(f,function(n,r,u,a){
var o="\t\r\n/+".includes(n[0]),f=o||!l++&&e.slice(c,a).toUpperCase()===e.slice(c,a).toLowerCase();return o&&(l=0,
c=a+1),
u=u?u.length>2&&u.slice(-1).toLowerCase()===u.slice(-1)&&!/^e?s\b/.test(e.substr(a+n.length-1,3))?t?i(u.slice(0,-2),true)+"-"+i(u.slice(-2),true):i(u[0],f)+i(u.slice(1,-2),true)+i(u.slice(-2,-1),false)+u.slice(-1):t?i(u,true):i(u[0],f)+i(u.slice(1),true):"",
(o?n[0]:(r||"")+(r||t&&!f?"-":""))+u}):a||o?e.replace(f,function(e){return i(e,false)}):e,
t&&(e=e.replace(r.we<64||false?/[a-z\u03b1-\u03c9]([A-Z\u0391-\u03a9]+[a-z\u03b1-\u03c9]?)/g:new RegExp("\\p{Ll}(\\p{Lu}+\\p{Ll})","ug"),function(n,r,u){
return r=r.length>2&&r.slice(-1).toLowerCase()===r.slice(-1)&&!/^e?s\b/.test(e.substr(u+n.length-1,3))?i(r.slice(0,-2),true)+"-"+i(r.slice(-2),true):i(r,true),
n[0]+"-"+r})),e},i=function(e){return e.replace(r.we<64||false?/[\u0300-\u0362]/g:new RegExp("\\p{Diacritic}","gu"),"")
},n.xr=function(e){if(null!=e.$sed)return e.$sed;var n=e.sed,r=e.sedKeys||e.sedKey
;return null!=n||r?n&&"object"==typeof n?null!=n.r||n.k?n:null:e.$sed={r:n,k:r}:null},s=function(e,n){var r,u,t,a,o,f
;if("object"==typeof e)return e.Zr||e.Dr?e:n?n.k=null:null
;for(r=null,u=0,t=0;t<e.length;t++)(o=-33&(a=e.charCodeAt(t)))>64&&o<91?u|=83===o?32772:1<<o-65:(r||(r=[]),
!n&&r.includes(a)||r.push(a));return f=u||r?{Zr:u,Dr:r}:null,n?n.k=f:f},p=function(e,n){var r,u;if(e.Zr&n.Zr)return true
;if(r=n.Dr,!e.Dr||!r)return false;for(u of e.Dr)if(r.includes(u))return true;return false},
n.doesNeedToSed=function(e,n){var u,t;if(n&&(false===n.r||n.r&&true!==n.r))return false!==n.r
;for(t of(u=n&&n.k&&s(n.k,n)||(e?{Zr:e,Dr:null}:null),o||u&&(o=f(r.ge.clipSub,null)),u?o:[]))if(p(t.zr,u))return true
;return false},d=function(e){
return e.includes("\n")?e:r.we>63||false?e.replace(new RegExp("(?<!\\\\) ([\\w\\x80-\\ufffd]{1,6})(?![\\x00- \\w\\\\\\x7f-\\uffff])","g"),"\n$1"):e.replace(/\\ | ([\w\x80-\ufffd]{1,6})(?![\x00- \w\\\x7f-\uffff])/g,function(e,n){
return" "===e[1]?e:"\n"+n})},r.f=function(e,n,a){var b,g,x,_,v=a&&"object"==typeof a?a.r:a;if(false===v)return e
;for(_ of(b=o||(o=f(r.ge.clipSub,null)),g=a&&"object"==typeof a&&a.k&&s(a.k,a)||(n?{Zr:n,Dr:null}:null),
v&&true!==v&&(g||(b=[]),b=f(d(v+""),g||(g={Zr:1073741824,Dr:null})).concat(b)),x=function(n){var r,a,o,f,s,d
;if(p(n.zr,g)&&(!n.Rr||("string"==typeof n.Rr&&(n.Rr=t.ut(n.Rr)||-1),-1!==n.Rr&&t.tt(n.Rr,e)))){if(r=-1,n.Er?(a=0,
e.replace(n.Ar,function(e){var n=arguments;return r=(a=n[n.length-2])+e.length,o=n.length>3?n[1]:"",""}),
r>=0&&(f=e.replace(n.Ar,n.Nr),e=f.slice(a,f.length-(e.length-r))||o||e.slice(a,r))):n.Ar.test(e)&&(r=n.Ar.lastIndex=0,
e=e.replace(n.Ar,n.Nr)),r<0)return"continue";if(!e)return"break";for(d of(s=false,
n.Fr))e=1===d?u.rn(e):2===d?u.en(e):19===d?u.en(e,true):3===d?l(e):4===d?e.toLocaleUpperCase():5===d?e.toLocaleLowerCase():10===d?u.tn(e):11===d?u.nn(e):12===d?encodeURI(e):13===d?encodeURIComponent(e):8===d?u.on(e,"atob"):9===d?btoa(e):(e=6!==d&&7!==d&&18!==d||false?e:e.normalize(18===d?"NFD":"NFC"),
7===d?Array.from(e).reverse().join(""):18===d?i(e):14===d||15===d||16===d||17===d?c(e,d):e),s=99===d;if(s)return"break"}
},g?b:[]))if("break"===x(_))break;return u.bn(),e},b=function(){var e=globalThis.document.createElement("textarea")
;return e.style.position="absolute",e.style.left="-99px",e.style.width="0",e},g=function(e,n,u){var t
;return"string"!=typeof e?(e=e.map(function(e){return r.f(e,32768,u)
}),e="string"==typeof n&&n.startsWith("json")?JSON.stringify(e,null,+n.slice(4)||2):e.join(n!==!!n&&n||"\n")+(e.length>1&&(!n||n===!!n)?"\n":""),
false!==(u&&"object"==typeof u?u.r:u)&&(e=r.f(e,4096)),
e):(32!==(t=(e=e.replace(/\xa0/g," ").replace(/\r\n?/g,"\n")).charCodeAt(e.length-1))&&9!==t||((t=e.lastIndexOf("\n")+1)?e=e.slice(0,t)+e.slice(t).trimRight():32!==(t=e.charCodeAt(0))&&9!==t&&(e=e.trimRight())),
e=r.f(e,4,u))},x=function(e,n){return e&&(e=e.replace(/\xa0/g," "),e=r.f(e,32768,n)),e},r.w=function(e,n,r){
if(e=g(e,n,r)){var u=globalThis.document,t=b();t.value=e,u.body.appendChild(t),t.select(),u.execCommand("copy"),
t.remove(),t.value=""}return e},r.h=r.e.ye?function(e,n){var t,a=globalThis.document,o=b();return o.maxLength=n||102400,
a.body.appendChild(o),o.focus(),a.execCommand("paste"),t=o.value.slice(0,n||102400),o.value="",o.remove(),
o.removeAttribute("maxlength"),
!n&&t.length>=81920&&("data:"===t.slice(0,5).toLowerCase()||u.ln(t))?r.h(e,20971520):x(t,e)}:function(){return null},
r.ce.clipSub=function(){o=null}});