"use strict"
;__filename="background/completion_utils.js",define(["require","exports","./store","./browser","./utils","./settings","./normalize_urls","./tools","./browsing_data_manager"],function(n,r,t,e,u,o,i,f,a){
var l,c,s,_,v,h,m,d,b,p,w,g,x,M,T,k,E,y,R,I;Object.defineProperty(r,"__esModule",{value:true}),
r.Bu=r.of=r.sortBy0=r.shortenUrl=r.highlight=r.cutTitle=r.Pu=r.get2ndArg=r.ComputeRelevancy=r.ComputeRecency=r.ComputeWordRelevancy=r.Au=r.zu=r.Ou=r.rf=r.ci=r.Gu=r._f=r.uf=r.setupQueryTerms=r.tabsInNormal=r.Gu=r.tabsInNormal=void 0,
u=u,o=o,l=[0,0],r.tabsInNormal=c=null,s=null,_=0,v=[],r.Gu=3,r.setupQueryTerms=function(n,r,t){h=n,m=r,b=false,d=t},
r.uf=function(n){h=n},r.ci={Tu:null,Ru:null,Xt:0,_o:0,jr:function(n){var e,u,o,i,f,a=null,l=0,c=h.join(" ");for(e=v,
u=c?e.length:0;0<=--u;)if(e[u].vo||!n){for(o=e[u].$t,i=0,f=0;i<o.length&&f<h.length;f++)h[f].includes(o[i])&&i++
;if(i>=o.length){a=e[u];break}}
r.ci.Tu=a,a&&(t.pe.t<200||!a.Pe||a.Pe.length>1e3)&&(l=performance.now())-a.Ri<Math.max(300,1.3*t.pe.t)?(r.ci.Ru=a,
a.$t=h.slice(0)):!c||a&&c===a.$t.join(" ")||!(c.length>4||/\w\S|[^\x00-\x80]/.test(c))?r.ci.Ru=null:(r.ci.Ru={
$t:h.slice(0),vo:n,Ri:l||performance.now(),Pe:a&&a.Pe,ve:a&&a.ve},v.push(r.ci.Ru),
r.ci.Xt||(r.ci.Xt=setInterval(r.ci.do,6e3)))},do:function(){
for(var n=v,t=-1,e=performance.now()-5983;++t<n.length&&n[t].Ri<e;);++t<n.length?n.splice(0,t):(n.length=0,
clearInterval(r.ci.Xt),r.ci.Xt=0)},Et:function(n){for(var r of v)n<2?r.Pe=null:n<3?r.ve=null:s=null},Vi:function(n){
s!==n&&(r.ci._o&&(clearTimeout(r.ci._o),r.ci._o=0),s=n,n&&(r.ci._o=setTimeout(r.ci.Vi,3e3,null)))}},r.rf={bo:0,Cr:0,
nf:function(){var n=h[0],e=t.fe.keywords;return null===e?(r.rf.Cr=r.rf.Cr||setTimeout(r.rf.go,67),
true):!(n.length>=r.rf.bo)&&e.includes("\n"+n)},go:function(){var n,e,o,i=u.gn(t.fe.map).sort(),f=0,a="",l=[]
;for(n=i.length;0<=--n;)a.startsWith(e=i[n])||(f=(o=e.length)>f?o:f,a=e,l.push(e));t.fe.keywords="\n"+l.join("\n"),
r.rf.bo=f,r.rf.Cr=0}},r.Ou={Iu:null,Qu:null,hf:null,vf:function(){var n,t=r.Ou.Iu=[];for(n of(r.Ou.Qu=r.Ou.hf=null,
h))t.push(new RegExp(u.t(n),n!==n.toUpperCase()&&n.toLowerCase()===n?"i":""))},Uu:function(){
var n,t,e,u=r.Ou.Qu=[],o=r.Ou.hf=[];for(n of r.Ou.Iu)t="\\b"+n.source,e=n.flags,u.push(new RegExp(t,e)),
o.push(new RegExp(t+"\\b",e))},pf:function(n){r.Ou.Iu&&(r.Ou.Iu[0]=new RegExp(u.t(n),r.Ou.Iu[0].flags))}},
r.zu=function(n,t){for(var e of r.Ou.Iu)if(!(e.test(n)||e.test(t)))return false;return true},p=function(n,t){
var e,u,o,i=0,f=0,a=0,l=0,c=!!t;for(r.Ou.Qu||r.Ou.Uu(),e=0,u=h.length;e<u;e++)l+=(o=g(e,n))[0],a+=o[1],
c&&(f+=(o=g(e,t))[0],i+=o[1]);return l=l/3*w(a,n.length),0===i?t?l/2:l:l<(f=f/3*w(i,t.length))?f:(l+f)/2},r.Au=p,
w=function(n,r){return n<r?n/r:r/n},g=function(n,t){var e,u=0;return(e=t.split(r.Ou.Iu[n]).length)<1?l:(u=1,
r.Ou.Qu[n].test(t)&&(u+=1,r.Ou.hf[n].test(t)&&(u+=1)),[u,(e-1)*h[n].length])},x=function(n){return r.Au(n.t,n.title)},
r.ComputeWordRelevancy=x,r.ComputeRecency=function(n){var r=(n-0)/18144e5
;return r<0?0:r<1?r*r*.666667:r<1.000165?.666446:0},M=function(n,t,e){var u=r.ComputeRecency(e),o=r.Au(n,t)
;return u<=o?o:(o+u)/2},r.ComputeRelevancy=M,r.get2ndArg=function(n,r){return r},T=function(n){var t,e,u
;m||void 0!==n.v||(n.v=r.of(n.u)),null==n.textSplit?(n.title=r.cutTitle(n.title),
(e=i.$e(t=n.t)).length!==t.length?u=E(t,"\\"===e[0]?5:"/"===t.charAt(7)&&"%3a"===t.substr(9,3).toLowerCase()?10:8):(e=r.shortenUrl(t),
u=y(e)),
n.t=t.length!==n.u.length?e:"",n.textSplit=R(e,u,t.length-e.length,m?d-13-Math.min(n.title.length,40):d)):n.t===n.u&&(n.t="")
},r.Pu=T,k=function(n,t){var e=n.length>d+40;return e&&(n=u.pn(n,0,d+39)),r.highlight(e?n+"\u2026":n,t||y(n))},
r.cutTitle=k,r.highlight=function(n,r){var t,e,o,i,f;if(b)return n;if(0===r.length)return u.mn(n);for(t="",e=0,
o=0;o<r.length;o+=2)f=r[o+1],(i=r[o])>=n.length||(t+=u.mn(n.slice(e,i)),t+="<match>",t+=u.mn(n.slice(i,f)),
t+="</match>",e=f);return t+u.mn(n.slice(e))},r.shortenUrl=function(n){var r=u.k(n)
;return!r||r>=n.length?n:n.slice(r,n.length-+(n.endsWith("/")&&!n.endsWith("://")))},E=function(n,r){var t,e=y(n)
;for(t=0;t<e.length;)e[t+1]<=r?e.splice(t,2):(e[t]=Math.max(e[t]-r,0),e[t+1]-=r,t+=2);return e},y=function(n){
var t,e,u,o,i,f,a,l,c,s,_,v=[];for(t=0,e=h.length;t<e;t++)for(u=0,o=0,i=void 0,a=(f=n.split(r.Ou.Iu[t])).length-1,
l=h[t].length;u<a;u++,o=i)i=(o+=f[u].length)+l,v.push([o,i]);if(0===v.length)return v;if(1===v.length)return v[0]
;for(v.sort(r.sortBy0),c=v[0],t=1,s=1,e=v.length;s<e;s++)c[t]>=(_=v[s])[0]?c[t]<_[1]&&(c[t]=_[1]):(c.push(_[0],_[1]),
t+=2);return c},r.sortBy0=function(n,r){return n[0]-r[0]},R=function(n,r,t,e){var o,i,f,a,l,c="",s=n.length,_=s,v=""
;if(s<=e||(t>1?_=n.indexOf("/")+1||s:(_=n.indexOf(":"))<0?_=s:u.wn.test(n.slice(0,_+3).toLowerCase())?_=n.indexOf("/",_+4)+1||s:_+=22),
_<s&&r.length)for(o=r.length,i=s+8;(o-=2)>-4&&i>=_;i=o<0?0:r[o])if(f=o<0?_:r[o+1],(a=i-20-Math.max(f,_))>0&&(s-=a)<=e){
_=f+(e-s);break}for(s=0,o=0;s<e&&o<r.length;o+=2)(a=(i=r[o])-20-(l=Math.max(s,_)))>0?(e+=a,v=u.pn(n,s,l+11),
c+=b?v:u.mn(v),c+="\u2026",v=u.dn(n,i-8,i),c+=b?v:u.mn(v)):s<i&&(v=n.slice(s,i),c+=b?v:u.mn(v)),v=n.slice(i,s=r[o+1]),
b?c+=v:(c+="<match>",c+=u.mn(v),c+="</match>");return v=n.length<=e?n.slice(s):u.pn(n,s,e-1>s?e-1:s+10),
c+(b?v:u.mn(v))+(n.length<=e?"":"\u2026")},r.of=function(n){
for(var r=a.Wr.hi&&n.startsWith("http")?a.Wr.gi(n):-1,e=r<0?~r-1:r,u=e<0?[]:t.ae.Pe,o=n.indexOf(":")+3,i=0,f=0,l="",c="",s=0,_=0;i<=e&&(o="/"===n[o]?o+1:n.indexOf("/",o+1)+(f?0:1))>0;f=o){
for(l=n.slice(f,o),_=e;i<=_;)if((c=u[s=i+_>>>1].u.slice(f))>l)_=s-1;else{if(c===l)return f?u[s].u:"";i=s+1}
if(i<=e&&f&&"/"===(l=u[i].u)[o]&&l.length<=++o)return l}return""},I=function(n,u,o,i,f){var a,l,v=t.ne
;null===c&&(v=1!==v?v:t.we>51||t.e.Ge||s?t.ne=0:1),1!==v||2048&u?(r.tabsInNormal=c=2!==v&&!(2048&u),
_!==(a=(c?2:0)|(n?1:0))&&(s=null,
_=a),(f=f||s)?o(i,f):(l=o.bind(null,i),n?(512&u?e.getCurTabs:e.Fn)(l):e.An.query({},l))):e.getCurWnd(n,function(e){
t.ne=e.incognito?2:0,i.o||r.Bu(n,u,o,i,n?e.tabs:null)})},r.Bu=I,f._t.Or=function(){
s&&(1&_||!(2&_)!=(2===t.ne))&&r.ci.Vi(null)},o.lo.then(function(){o.co("searchEngines",null)})});