"use strict"
;__filename="background/exclusions.js",define(["require","exports","./store","./utils","./browser","./normalize_urls","./settings","./ports"],function(n,t,u,o,r,e,i,l){
var f,a,c,s,v,m,p,d,$,g;Object.defineProperty(t,"__esModule",{value:true
}),t.Yn=t.Zn=t.Jn=t.nt=t.Un=t.Gn=t.tt=t.ut=t.ot=void 0,o=o,i=i,t.ot=function(n,t){var u,r
;return t=t&&t.replace(/<(\S+)>/g,"$1"),
"^"===n[0]?(u=o.d(n.startsWith("^$|")?n.slice(3):n,"",0))||console.log("Failed in creating an RegExp from %o",n):"`"===n[0]&&((r=o.m(n.slice(1),0))||console.log("Failed in creating an URLPattern from %o",n)),
u?{t:1,v:u,k:t}:r?{t:3,v:r,k:t}:{t:2,v:n.startsWith(":vimium://")?e.We(n.slice(10),false,-1):n.slice(1),k:t}},
t.ut=function(n){var t,u,r,i;return"^"===n[0]?(n=n.startsWith("^$|")?n.slice(3):n,
t=".*$".includes(n.slice(-2))?n.endsWith(".*$")?3:n.endsWith(".*")?2:0:0,n=0!==t&&"\\"!==n[n.length-t]?n.slice(0,-t):n,
(u=o.d(n,""))?{t:1,v:u}:null):"`"===n[0]?(r=o.m(n.slice(1)))?{t:3,v:r
}:null:"localhost"===n||!n.includes("/")&&n.includes(".")&&(!/:(?!\d+$)/.test(n)||o.cn(n,6))?(n=(n=(n=n.toLowerCase()).endsWith("*")?n.slice(0,/^[^\\]\.\*$/.test(n.slice(-3))?-2:-1):n).startsWith(".*")&&!/[(\\[]/.test(n)?"*."+n.slice(2):n,
i=void 0,
(u=o.d("^https?://"+(n.startsWith("*")&&"."!==n[1]?"[^/]"+n:(i=n.replace(/\./g,"\\.")).startsWith("*")?i.replace("*\\.","(?:[^./]+\\.)*?"):i),"",0))?{
t:1,v:u}:n.includes("*")?null:{t:2,v:"https://"+(n.startsWith(".")?n.slice(1):n)+"/"}):{t:2,
v:(t=(n=(n=(":"===n[0]?n.slice(1):n).replace(/([\/?#])\*$/,"$1")).startsWith("vimium://")?e.We(n.slice(9),false,-1):n).indexOf("://"))>0&&t+3<n.length&&n.indexOf("/",t+3)<0?n+"/":n
}},t.tt=function(n,t){return 1===n.t?n.v.test(t):2===n.t?t.startsWith(n.v):n.v.test(t)},t.Gn=f=false,t.Un=a=false,
c=false,s=[],v=function(n){s=n.map(function(n){return t.ot(n.pattern,n.passKeys)})},m=function(n){
return(n?[t.ot(n,"")]:s).map(function(n){return{t:n.t,v:1===n.t?n.v.source:2===n.t?n.v:{hash:n.v.hash,
hostname:n.v.hostname,pathname:n.v.pathname,port:n.v.port,protocol:n.v.protocol,search:n.v.search}}})},t.nt=m,
p=function(n,r){var e,i,l,f,a="";for(i of s)if(1===i.t?i.v.test(n):2===i.t?n.startsWith(i.v):i.v.test(n)){
if(0===(l=i.k).length||"^"===l[0]&&l.length>2||c)return l&&l.trim();a+=l}
return!a&&r.Cn&&n.lastIndexOf("://",5)<0&&!o.wn.test(n)&&null!=(f=null===(e=u.ie.get(r.Hn))||void 0===e?void 0:e.Qn)?t.Jn(f.s.or,f.s):a?a.trim():null
},t.Jn=p,d=function(){var n=!r.On()||false?null:function(n){u.R[8](n)};return d=function(){return n},n},t.Zn=function(){
var n,t,u,r=o.fn(),e=0;for(n of s)if(t=n.k){if("^"===t[0]&&t.length>2)return true;for(u of t.split(" "))r[u]=1,e++}
return e?r:null},$=function(n){var o,r,e=s.length>0?null:{N:1,p:null,f:0};n?e||i.so({N:3,H:8
}):(o=null!=u.y||void 0!==u.y&&u.H,r=s,l.po(function(n){var r,i,l,f=n.Ln.s.je,a=n.Ln.s;for(r of n.wo){if(i=null,l=0,e){
if(0===r.s.je)continue}else if(l=null===(i=t.Jn(r.s.or,r.s))?0:i?1:2,!i&&r.s.je===l)continue;n.Dn||(r.postMessage(e||{
N:1,p:i,f:0}),r.s.je=l)}o&&f!==a.je&&u.C(a.Hn,a.je)},function(){return r===s}))},t.Yn=$,g=function(){
var n,o,e=s.length>0,i=e||f?d():null;i&&(f!==e&&(t.Gn=f=e,n=r.On().onHistoryStateUpdated,
e?n.addListener(i):n.removeListener(i)),a!==(o=e&&u.ge.exclusionListenHash)&&(t.Un=a=o,
n=r.On().onReferenceFragmentUpdated,o?n.addListener(i):n.removeListener(i)))},u.ce.exclusionRules=function(n){
var o=!s.length,r=u.X;v(n),c=u.ge.exclusionOnlyFirstMatch,g(),setTimeout(function(){setTimeout(t.Yn,10,o),
u.X===r&&i.co("keyMappings",null)},1)},u.ce.exclusionOnlyFirstMatch=function(n){c=n},u.ce.exclusionListenHash=g,
i.lo.then(function(){v(u.ge.exclusionRules),c=u.ge.exclusionOnlyFirstMatch})});