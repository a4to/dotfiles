"use strict"
;__filename="background/frame_commands.js",define(["require","exports","./store","./utils","./browser","./normalize_urls","./ports","./ui_css","./i18n","./key_mappings","./run_commands","./open_urls","./tools"],function(n,t,e,l,i,o,u,r,a,f,s,c,m){
var v,d,p,g,h;Object.defineProperty(t,"__esModule",{value:true
}),t.focusFrame=t.framesGoNext=t.toggleZoom=t.mainFrame=t.framesGoBack=t.openImgReq=t.captureTab=t.enterVisualMode=t.showVomnibar=t.initHelp=t.performFind=t.parentFrame=t.nextFrame=void 0,
l=l,v=function(){var n,l=e.U,i=-1,o=e.ie.get(l.s.Hn),u=o&&o.wo;if(u&&u.length>1){for(i=u.indexOf(l),
n=Math.abs(e.T);n>0;n--)(i+=e.T>0?1:-1)===u.length?i=0:i<0&&(i=u.length-1);l=u[i]}
t.focusFrame(l,0===l.s.Cn,l!==e.U&&o&&l!==o.Ln?3:2,e.z)},t.nextFrame=v,d=function(){
var n=e.U.s,l=n.Hn>=0&&e.ie.get(n.Hn)?null:"Vimium C can not access frames in current tab";l&&u.showHUD(l),
u.getParentFrame(n.Hn,n.Cn,e.T).then(function(n){n?t.focusFrame(n,true,4,e.z):t.mainFrame()})},t.parentFrame=d,
t.performFind=function(){
var n=e.U.s,t=e.T<0?-e.T:e.T,l=e.z.index,i=l?"other"===l?t+1:"count"===l?t:l>=0?-1-(0|l):0:0,o=!!i||!e.z.active,u=null
;32&n.Kn||(n.Kn|=32,u=r.ni(n)),s.sendFgCmd(1,true,s.wrapFallbackOptions({c:i>0?e.T/t:e.T,l:o,f:u,m:!!e.z.highlight,
n:!!e.z.normalize,r:true===e.z.returnToViewport,s:!i&&t<2&&!!e.z.selected,p:!!e.z.postOnEsc,e:!!e.z.restart,
q:e.z.query?e.z.query+"":o||e.z.last?m.Ct.$t(n.Sn,"",i<0?-i:i):""}))},t.initHelp=function(n,t){var o=e.Y||[]
;return Promise.all([i.import2(e.e.HelpDialogJS),null!=o[0]?null:l.a("help_dialog.html"),null!=o[1]?null:a.getI18nJson("help_dialog"),null!=o[2]?null:a.getI18nJson("params.json")]).then(function(l){
var i,o,u=l[0],r=l[1],a=l[2],c=l[3],m=n.w&&(null===(i=e.ie.get(t.s.Hn))||void 0===i?void 0:i.Qn)||t,v=m.s.or.startsWith(e.e.Ee),d=n.a||{}
;m.s.Kn|=64,e.U=m,o=e.Y||(e.Y=[null,null,null]),r&&(o[0]=r),a&&(o[1]=a),c&&(o[2]=c),s.sendFgCmd(17,true,{
h:u.nl(v,d.commandNames),o:e.e.Ee,f:n.f,e:!!d.exitOnClick,c:v&&!!f.mt||e.ge.showAdvancedCommands})},null)},
t.showVomnibar=function(n){var t,i,o,u,r,a,f,c,m,v,d=e.U,p=e.z.url;if(null!=p&&true!==p&&"string"!=typeof p&&(p=null,
delete e.z.url),!d){if(!(d=(null===(t=e.ie.get(e.oe))||void 0===t?void 0:t.Qn)||null))return;e.U=d}
"bookmark"===e.z.mode&&s.overrideOption("mode","bookm"),o=d.s.or,u=!(i=e.vomnibarPage_f).startsWith(e.e.xe),
r=o.startsWith(e.e.xe),
a=n||!i.startsWith(location.origin+"/")?e.e.ze:i,n=n||(u?r||i.startsWith("file:")&&!o.startsWith("file:///")||i.startsWith("http:")&&!/^http:/.test(o)&&!/^http:\/\/localhost[:/]/i.test(i):d.s.Sn||r&&!i.startsWith(o.slice(0,o.indexOf("/",o.indexOf("://")+3)+1))),
m=e.z.trailing_slash,null==(v=s.copyCmdOptions(l.sn({v:(f=n||i===a||d.s.Hn<0)?a:i,i:f?null:a,t:f?0:u?2:1,
s:null!=(c=e.z.trailingSlash)?!!c:null!=m?!!m:null,j:f?"":e.e.Ke,e:!!e.z.exitOnClick,k:l.n(true)
}),e.z)).icase&&e.ge.vomnibarOptions.actions.includes("icase")&&(v.icase=true),s.portSendFgCmd(d,6,true,v,e.T),
v.k="omni",e.z=v},t.enterVisualMode=function(){var n,t,l,i,o,u,a;t="string"==typeof(n=e.z.mode)?n.toLowerCase():"",
i=null,o="",u=null,a=null,16&~(l=e.U.s).Kn&&(o=e.G,32&l.Kn||(l.Kn|=32,i=r.ni(l)),u=f.et,a=f.at,l.Kn|=16),
s.sendFgCmd(5,true,s.wrapFallbackOptions({m:"caret"===t?3:"line"===t?2:1,f:i,g:a,k:u,t:!!e.z.richText,s:!!e.z.start,w:o
}))},t.captureTab=function(n,t){
var l=e.z.show,o=false===e.z.download,u=!!e.z.png||false?0:Math.min(Math.max(0|e.z.jpeg,0),100),r=n&&n[0],a=r?r.id:e.oe,f=r?r.title:""
;f="title"===e.z.name||!f||a<0?f||""+a:a+"-"+f,f+=u>0?".jpg":".png",i.An.captureVisibleTab(r?r.windowId:e.te,u>0?{
format:"jpeg",quality:u}:{format:"png"},function(n){var u,r,a;if(!n)return t(0),i.Mn();u=function(n){
var u,s,c=o&&!l?null:"string"!=typeof n?URL.createObjectURL(n):n;if(c&&c.startsWith("blob:")&&(p&&(clearTimeout(p[0]),
URL.revokeObjectURL(p[1])),p=[setTimeout(function(){p&&URL.revokeObjectURL(p[1]),p=null},l?5e3:3e4),c]),l)return r(c),
void t(1);o||(s=e.U&&(null===(u=e.ie.get(e.U.s.Hn))||void 0===u?void 0:u.Qn)||e.U,
i.downloadFile(c,f,s?s.s.or:null,function(n){n||a(c),t(n)}))},r=function(n){e.R[23]({o:"pixel=1&",u:n,f:f,a:false,m:36,
q:{r:e.z.reuse,m:e.z.replace,p:e.z.position,w:e.z.window}},e.U)},a=function(n){
var t=globalThis.document.createElement("a");t.href=n,t.download=f,t.target="_blank",t.click()},
n.startsWith("data:")?fetch(n).then(function(n){return n.blob()}).then(u):u(n)})},t.openImgReq=function(n,t){
var i,r,f,m,v,d,p,g,h,_,b=n.u;if(/^<svg[\s>]/i.test(b)){
if(r=(new DOMParser).parseFromString(b,"image/svg+xml").firstElementChild)for(f of(r.removeAttribute("id"),
r.removeAttribute("class"),[].slice.call(r.querySelectorAll("script,use"))))f.remove()
;if(!r||!r.lastElementChild)return e.U=t,void u.showHUD(a.Xn("invalidImg"))
;r.setAttribute("xmlns","http://www.w3.org/2000/svg"),b="data:image/svg+xml,"+encodeURIComponent(r.outerHTML),
n.f=n.f||"SVG Image"}if(!l.an(b))return e.U=t,void u.showHUD(a.Xn("invalidImg"));m=e.e.Ue+"#!image ",
n.f&&(m+="download="+l.nn(n.f)+"&"),false!==n.a&&(m+="auto=once&"),n.o&&(m+=n.o),d=(v=n.q||l.fn()).k,
p=null!==(i=v.t)&&void 0!==i?i:!d,h=(g=v.s?e.f(b,32768,v.s):b)!==b,b=g,s.replaceCmdOptions({opener:true,
reuse:null!=v.r?v.r:16&n.m?-2:-1,replace:v.m,position:v.p,window:v.w
}),e.T=1,_=d||h?p?o.er(b,d,2):o.Qe(b.trim().split(l.hn),d,2):b,
c.openUrlWithActions("string"!=typeof _||!p||_.startsWith(location.protocol)&&!_.startsWith(location.origin+"/")?_:m+_,9)
},g=function(n,l,o){var r,f,m,v,d,p,g,h,_,b=!!i.An.goBack
;if(!b&&(o?i.getTabUrl(o):(l.s.Cn?e.ie.get(l.s.Hn).Qn:l).s.or).startsWith(e.e.xe)&&true)return e.U=l,
u.showHUD(a.Xn("noTabHistory")),void s.runNextCmd(0);if(r=s.hasFallbackOptions(n.o)?(s.replaceCmdOptions(n.o),
s.getRunNextCmdBy(0)):i.Mn,f=function(n,t){i.An.executeScript(n.id,{code:"history.go(".concat(t,")"),
runAt:"document_start"},r)},m=o?o.id:l.s.Hn,v=n.s,d=c.parseReuse(n.o.reuse||0))p=n.o.position,
i.An.duplicate(m,function(e){var l,o,u;if(!e)return r()
;-2===d&&i.selectTab(m),b?((l=s.parseFallbackOptions(n.o)||{}).reuse=0,t.framesGoBack({s:v,o:l},null,e)):f(e,v),
o=e.index--,null!=(u="end"===p?3e4:c.newTabIndex(e,p,false,true))&&u!==o&&i.An.move(e.id,{index:u},i.Mn)
});else if(g=v>0?i.An.goForward:i.An.goBack,b||g)for(h=0,_=v>0?v:-v;h<_;h++)g(m,h?i.Mn:r);else f(o,v)},t.framesGoBack=g,
h=function(){var n=e.ie.get(e.U?e.U.s.Hn:e.oe),l=n&&n.Qn
;l&&l===n.Ln&&e.z.$else&&"string"==typeof e.z.$else?s.runNextCmd(0):l&&t.focusFrame(l,true,l===n.Ln?2:4,e.z)},
t.mainFrame=h,t.toggleZoom=function(n){i.jn(i.An.getZoom).then(function(t){var l,o,u,r,a,f,s,c,m,v,d;if(t){
if(l=e.T<-4?-e.T:e.T,(e.z.in||e.z.out)&&(l=0,e.T=e.z.in?e.T:-e.T),u=e.z.level,r=Math,
e.z.reset)o=1;else if(null!=u&&!isNaN(+u)||l>4)a=r.max(.1,r.min(0|e.z.min||.25,.9)),
f=r.max(1.1,r.min(0|e.z.min||5,100)),o=null==u||isNaN(+u)?l>1e3?1:l/(l>49?100:10):1+u*e.T,o=r.max(a,r.min(o,f));else{
for(s=0,
c=9,m=[.25,1/3,.5,2/3,.75,.8,.9,1,1.1,1.25,1.5,1.75,2,2.5,3,4,5],v=0,d=0;v<m.length&&(d=Math.abs(m[v]-t))<c;v++)s=v,c=d
;o=m[s+e.T<0?0:r.min(s+e.T,m.length-1)]}Math.abs(o-t)>.005?i.An.setZoom(o,i.zn(n)):n(0)}else n(0)})},
t.framesGoNext=function(n,t){var l,i,o,u,r=e.z.patterns,a=false
;if(r&&r instanceof Array||(r=(r=r&&"string"==typeof r?r:(a=true,
n?e.ge.nextPatterns:e.ge.previousPatterns)).split(",")),a||!e.z.$fmt){for(i of(l=[],
r))if((i=i&&(i+"").trim())&&l.push(".#[".includes(i[0])?i:i.toLowerCase()),200===l.length)break;r=l,
a||(s.overrideOption("patterns",r),s.overrideOption("$fmt",1))}o=r.map(function(n){
return Math.max(n.length+12,4*n.length)}),u=Math.max.apply(Math,o),s.sendFgCmd(10,true,s.wrapFallbackOptions({
r:e.z.noRel?"":t,n:n,exclude:e.z.exclude,match:e.z.match,evenIf:e.z.evenIf,p:r,l:o,m:u>0&&u<99?u:32}))},
t.focusFrame=function(n,t,l,i){n.postMessage({N:7,H:t?u.ensureInnerCSS(n.s):null,m:l,k:e.F,c:0,
f:i&&s.parseFallbackOptions(i)||{}})}});