"use strict"
;__filename="background/i18n.js",define(["require","exports","./store","./utils","./browser"],function(n,r,u,t,e){
var o,i,c,f,a,l,s;Object.defineProperty(r,"__esModule",{value:true
}),r.cr=r.getI18nJson=r.dr=r.vr=r.Xn=r.br=r.gr=r.$r=void 0,r.$r=1,i=0,r.gr=[],r.br=function(n){
return e.Bn.i18n.getMessage(n)},c=function(n,u){if(1===i){var t=o.get(n)
;return null!=u&&t?t.replace(/\$\d/g,function(n){return u[+n[1]-1]}):t||""}return i||(i=r.getI18nJson("background")),
i.then(function(n){o=n,i=1}).then(r.Xn.bind(null,n,u))},r.Xn=c,f=function(n,r){return n.endsWith(r)},r.vr=function(n,r){
return n&&n.split(" ").reduce(function(n,u){return n||(u.includes("=")?r&&u.startsWith(r)?u.slice(r.length+1):n:u)},"")
},a=function(n){var u=r.br("i18n");return r.vr(u,n||"background")||r.br("lang1")||"en"},r.dr=a,l=function(n){
var u=f(n,".json")?n:"".concat(r.dr(n),"/").concat(n,".json");return t.a("/i18n/".concat(u))},r.getI18nJson=l,
s=function(){var n,u=r.gr,t=["$1","$2","$3","$4"];for(n=0;n<117;n++)u.push(e.Bn.i18n.getMessage(""+n,t));r.cr=null},
r.cr=s});