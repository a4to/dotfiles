"use strict"
;__filename="background/main.js",define(["require","exports","./store","./utils","./browser","./settings","./ports","./key_mappings","./run_commands","./normalize_urls","./parse_urls","./exclusions","./ui_css","./eval_urls","./open_urls","./all_commands","./request_handlers","./tools"],function(n,o,e,t,i,u,s,r,c){
var a;Object.defineProperty(o,"__esModule",{value:true}),t=t,u=u,a=function(n){var o,t=e.ie.get(e.oe)
;"quickNext"===n&&(n="nextTab"),
(o=r.pt)&&o.get(n)?null==t||4&t.Kn||e.oe<0?c.executeShortcut(n,t):i.tabsGet(e.oe,function(o){
return c.executeShortcut(n,o&&"complete"===o.status?e.ie.get(o.id):null),i.Mn()}):o&&null!==o.get(n)&&(o.set(n,null),
console.log("Shortcut %o has not been configured.",n))},e.B=function(){
if(6===e.D)return e.B?(t.s(u.lo.then.bind(u.lo,e.B)),void(e.B=null)):(e.X||(u.co("keyMappings"),
0===e.o&&(r.et["m-s-c"]=36)),u.co("exclusionListenHash"),u.co("vomnibarOptions"),u.co("autoDarkMode"),
u.co("autoReduceMotion"),i.Bn.runtime.onConnect.addListener(function(n){return s.OnConnect(n,0|n.name)}),
i.Bn.runtime.onConnectExternal.addListener(function(n){var o,t=n.sender,i=n.name
;if(t&&s.isExtIdAllowed(t)&&i.startsWith("vimium-c.")&&(o=i.split("@")).length>1){
if(o[1]!==e.e.GitVer)return n.postMessage({N:2,t:1}),void n.disconnect();s.OnConnect(n,32|o[0].slice(9))
}else n.disconnect()}),void i.Bn.extension.isAllowedIncognitoAccess(function(o){e.e.Ge=false===o,setTimeout(function(){
new Promise(function(o,e){n(["/background/others.js"],o,e)}).then(n=>n),setTimeout(function(){new Promise(function(o,e){
n(["/background/browsing_data_manager.js"],o,e)}).then(n=>n),new Promise(function(o,e){
n(["/background/completion_utils.js"],o,e)}).then(n=>n),new Promise(function(o,e){n(["/background/completion.js"],o,e)
}).then(n=>n)},200)},200)}))},i.Bn.commands.onCommand.addListener(a),u.lo.then(function(){u.co("extAllowList"),
i.Bn.runtime.onMessageExternal.addListener(function(n,o,t){if(s.isExtIdAllowed(o))if("string"!=typeof n){
if("object"==typeof n&&n)switch(n.handler){case"shortcut":var i=n.shortcut;i&&a(i+"");break;case"id":t({name:"Vimium C",
host:location.host,shortcuts:true,injector:e.e.Ae,version:e.e.Oe});break;case 99:t({s:n.scripts?e.e.He:null,
version:e.e.Oe,host:"",h:"@"+e.e.GitVer});break;case"command":c.executeExternalCmd(n,o)}}else c.executeExternalCmd({
command:n},o);else t(false)}),u.co("vomnibarPage",null),u.co("searchUrl",null)}),
i.An.onReplaced.addListener(function(n,o){var t,i=e.ie.get(o);if(i)for(t of(e.ie.delete(o),e.ie.set(n,i),i.wo))t.s.Hn=n
}),e.j.yu=function(n,o,t){setTimeout(function(){e.j.yu(n,o,t)},210)},e.D=4|e.D,e.B(),globalThis.onunload=function(n){
if(!n||n.isTrusted){for(var o of e.ue)o.disconnect();e.ie.forEach(function(n){for(var o of n.wo.slice(0))o.disconnect()
})}},globalThis.window||(globalThis.onclose=onunload),e.we<59||!t.d("\\p{L}","u",0)?t.a("words.txt").then(function(n){
e.G=n.replace(/[\n\r]/g,"").replace(/\\u(\w{4})/g,function(n,o){return String.fromCharCode(+("0x"+o))})}):e.G=""});