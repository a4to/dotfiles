"use strict"
;__filename="background/open_urls.js",define(["require","exports","./store","./utils","./browser","./normalize_urls","./parse_urls","./ports","./exclusions","./i18n","./key_mappings","./run_commands","./clipboard","./tools"],function(n,u,r,e,t,i,l,o,f,c,a,s,d,v){
var p,m,w,g,b,y,h,I,$,k,P,_,T,x,U,j,z,L,M,N;Object.defineProperty(u,"__esModule",{value:true}),
u.du=u.openUrlReq=u.openUrl=u.goToNextUrl=u.openUrlWithActions=u.openShowPage=u.openJSUrl=u.parseReuse=u.pu=u.parseOpenPageUrlOptions=u.preferLastWnd=u.newTabIndex=void 0,
e=e,p={current:0,reuse:1,newwnd:2,frame:3,newbg:-2,lastwndfg:-5,lastwnd:-5,lastwndbg:-6,iflastwnd:-7,lastwndbgbg:-8,
lastwndbginactive:-8},u.newTabIndex=function(n,u,r,e){
return"before"===u||"left"===u||"prev"===u||"previous"===u?n.index:"after"===u||"next"===u||"right"===u?n.index+1:"default"===u?void 0:false!==e&&null!=t.getGroupId(n)?"start"===u||"begin"===u?n.index:"end"===u?void 0:n.index+1:"start"===u||"begin"===u?0:"end"===u?r?3e4:void 0:n.index+1
},u.preferLastWnd=function(n){return n.find(function(n){return n.id===r.le})||n[n.length-1]},
u.parseOpenPageUrlOptions=function(n){return{g:n.group,i:n.incognito,m:n.replace,o:n.opener,r:n.reuse,p:n.position,
w:n.window}},m=function(n){
return"boolean"==typeof n?n:n?"force"===n||("reverse"===n?2!==r.ne:"same"===n||"keep"===n?2===r.ne:null):null},
w=function(n){return"popup"===n||"normal"===n?n:void 0},g=function(n,u,e){var i=function(u){
return!(n&&u.type!==n||null!=e&&u.incognito!==e||"minimized"===u.state)}
;return(r.le<0?Promise.resolve(null):new Promise(function(n){t.$n.get(r.le,function(u){return n(u?i(u)?u:null:(r.le=-1,
null)),t.Mn()})})).then(function(n){return n||new Promise(function(n){return t.$n.getAll(function(e){
var t=e.filter(function(n){return i(n)&&n.id!==r.te}),l=t.length>0?t.sort(function(n,u){return u.id-n.id})[0]:null
;l&&r.le<0&&(r.le=l.id),n(l||!u?l:e.find(function(n){return n.id===r.te})||e.find(function(n){return n.focused})||null)
})})})},u.pu=function(n,u){n=n.slice(0,128).split("?")[0].replace(/\\/g,"/")
;var e=2===r.o&&/\/globalroot\/device\/condrv|\bdevice\/condrv\/kernelconnect/.test(n);return e&&(r.U=u||r.U,
o.showHUD(c.Xn("harmfulURL"))),e},b=function(n,l,f,c){var d,v,p,m,w=function(n){s.replaceCmdOptions(l),
s.overrideCmdOptions({urls:n,url:null,url_f:null,copied:null,keyword:null},true)};switch(e.bn(),c[1]){case 1:
o.showHUD(c[0],15),s.runNextCmdBy(1,l);break;case 5:case 7:w(null),7===c[1]||l.$p?n=0:s.overrideOption("$p",1),
u.openUrlWithActions(i.er(c[0]),n,false,f);break;case 4:n>=3&&c[0]&&s.runNextCmdBy(1,l);break;case 6:if(v=r.oe,
"openUrls"===(d=c[0])[0]){for(m of(p=[],d.slice(1)))"string"==typeof m||5!==m[1]&&7!==m[1]||(m=i.er(c[0],null,n)),
"string"!=typeof m?Promise.resolve(m).then(function(u){6===u[1]&&"openUrls"===u[0][0]||b(n,l,f,u)}):p.push(m)
;if(0===p.length)return;return w(p),void(f&&f.length>0?U(f):t.getCurTab(U))}setTimeout(function(){
var n=r.ie.get(v),u=e.sn({keys:[d[1]]});r.L=null,s.executeCommand(a.ct("runKey",u),1,0,n?n.Ln:null,0,null)},0)}},
y=function(n,u,r){n?s.runNextOnTabLoaded(u,r):s.runNextCmdBy(0,u)},h=function(n,u,e,i,l){var o=function(u){
return y(u,n,u),t.Mn()};if(l){if(l.length>0&&l[0].incognito&&t.xn(e))return void t.tabsCreate({url:e},o)
}else if(t.xn(e)&&true!==i)return void t.getCurTab(h.bind(null,n,u,e,true));3===u&&r.U&&r.U.s.Cn?s.sendFgCmd(0,false,{
r:1,u:e}):l?t.tabsUpdate(l[0].id,{url:e},o):t.tabsUpdate({url:e},o)},I=function(n,u,r,e,i,l){t.makeWindow({url:n,
focused:u,incognito:r,type:"string"==typeof n||1===n.length?w(e.window):void 0,left:i.left,top:i.top,width:i.width,
height:i.height},null!=i.state?i.state:l&&"minimized"!==l.state?l.state:"",function(n){y(n,e,n&&n.tabs[0])})},
$=function(n,e,i,l,o){var f,c,a=-2!==e,s=l?l.windowId:r.te,d=o.find(function(n){return n.id===s}),v=null!=d&&d.incognito
;!i.window&&2!==e&&(v||(o=o.filter(function(n){return n.incognito&&"normal"===n.type})).length>0)?(f={url:n[0],active:a,
windowId:v?s:u.preferLastWnd(o).id},v&&(f.index=u.newTabIndex(l,i.position,c=true===i.opener,i.group),
c&&(f.openerTabId=l.id)),t.openMultiTabs(f,r.T,true,n,v&&i.group,l,function(n){!v&&a&&t.selectWnd(n),y(n,i,n)
})):I(n,true,true,i,i,d)},u.parseReuse=function(n){
return null==n?-1:"string"!=typeof n?"boolean"==typeof n?n?1:-1:isNaN(+n)?-1:0|n:(n=n.toLowerCase().replace("window","wnd").replace(/-/g,""))in p?p[n]:-1
},k=function(n,u,i){
var l,o,f,c,a,s,d,v,p=u&&u.length>0?t.getTabUrl(u[0]):"",m=[true!==i?false===i?"":i:(/[%$]s/i.exec(n)||["${url_mask}"])[0],r.z.host_mask||r.z.host_mark,r.z.tabid_mask||r.z.tabId_mask||r.z.tabid_mark||r.z.tabId_mark,r.z.title_mask||r.z.title_mark,r.z.id_mask||r.z.id_mark||r.z.id_marker],w=[]
;for(l=0;l<m.length;l++)if((f=(o=null!=m[l]?m[l]+"":"")?n.indexOf(o):-1)>=0){for(a of(c=f+o.length,w));
w.push([f,c,0===l?/^[%$]S|^\$\{S:/.test(o)?p:e.nn(p):1===l?new URL(p).host:2===l?p&&""+u[0].id:3===l?p&&""+e.nn(u[0].title):t.Bn.runtime.id])
}if(w.length){for(v of(s="",d=0,w.sort(function(n,u){return n[0]-u[0]}),w))s=s+n.slice(d,v[0])+v[2],d=v[1]
;n=s+n.slice(d)}return n},P=function(n,e,i,l){var o=m(l.incognito);(e>-4?new Promise(function(n){
t.getCurWnd(false,function(u){return n(u||null),t.Mn()})}):g(w(l.window),true,o)).then(function(n){
return n&&new Promise(function(u){t.An.query({active:true,windowId:n.id},function(r){return n.tabs=r,u(n),t.Mn()})})
}).then(function(f){var c,a=!!f&&!f.focused&&f.id!==r.te,d=-7===e&&!a
;if(f&&(a||-7===e&&(null==o||f.incognito===!!o)))c=f.tabs&&f.tabs.length>0?t.selectFrom(f.tabs):null,t.openMultiTabs({
url:n[0],active:e>-6||d,windowId:f.id,pinned:!!l.pinned,index:c?u.newTabIndex(c,l.position,false,l.group):void 0
},r.T,!!l.incognito&&"string"==typeof l.incognito,n,l.group,c,function(n){
e>-6?a&&t.selectWnd(n):n&&e>-8&&!d&&t.selectTab(n.id),y(n,l,e>-6&&-2!==e&&n)});else{
if(-7===e&&s.runNextCmdBy(0,l))return;I(n,e>-8,null!=o?!!o:i,l,l,f)}})},_=function(n,e,i,l){
var o,f,c=l&&l[0],a=!!c&&c.incognito||2===r.ne,s=-2!==e&&-8!==e,d=2===e||e<-3||!!i.window,v=m(i.incognito),p=null!=v&&"string"==typeof i.incognito
;if(!p&&n.some(t.xn))d=a||d;else if(a)d=false===v||d;else if(v&&e>-4)return void t.$n.getAll($.bind(null,n,e,i,c))
;d?P(n,e,a,i):(f={url:n[0],active:s,windowId:c?c.windowId:void 0,openerTabId:o=i.opener&&c?c.id:void 0,
pinned:!!i.pinned,index:c?u.newTabIndex(c,i.position,null!=o,i.group):void 0
},t.openMultiTabs(f,r.T,p,n,i.group,c,function(n){s&&n&&t.selectWndIfNeed(n),y(n,i,s&&n)}))},T=function(n,e,i,l,o,c){
var a,d=i?"string"==typeof i?f.ut(i):"object"==typeof i&&i.t&&i.v?i:null:null,v=2===e||1===e,p=1===e&&o.q||{},b=w(1===e?p.w:l.window),h=m(1!==e?l.incognito:p.i),I=true===(1!==e?l.group:p.g)
;r.T=1,1===e?(p.m=null,p.g=I):(s.overrideOption("group",I,l),null!=l.replace&&s.overrideOption("replace",d,l)),
a=e<-3&&d?g(b,-7===e,h):Promise.resolve(!v&&r.te>=0?{id:r.te}:null),Promise.all([a,!I||c?null:new Promise(function(n){
t.getCurTab(function(u){c=u||[],n()})})]).then(function(n){var u=n[0];return d&&(u||v)?new Promise(function(n){
t.An.query(u?{windowId:u.id}:{windowType:b||void 0},function(u){
var i,l,o=null!=h?!h:e>-4?2!==r.ne:-2,a=(u||[]).filter(function(n){return f.tt(d,n.url)&&n.incognito!==o})
;return I&&a.length>0&&c.length>0&&(i=t.getGroupId(c[0]),u&&(a=a.filter(function(n){return t.getGroupId(n)===i}))),
a.sort(function(n,u){var e=r.ee.get(u.id),t=r.ee.get(n.id);return t?e?e.i-t.i:1:e?-1:u.id-n.id}),
1===e&&(l=a.filter(function(n){return n.windowId===r.te}),a=l.length>0?l:a),n(a.length?a[0]:null),t.Mn()})}):null
}).then(function(i){var f,a
;null==i||i.id===r.oe&&1!==e?1===e?u.du(o):s.runNextCmdBy(0,l)||(c?_([n],e,l,c):t.getCurTab(_.bind(null,[n],e,l))):(f=-2!==e&&-8!==e,
a=i.windowId!==r.te&&e>-6,t.tabsUpdate(i.id,{url:n},function(n){return n&&(f&&(t.selectTab(n.id),n.active=true),
a&&t.selectWnd(n)),y(n,1===e?o.f||{}:l,-2!==e&&e>-6&&n),t.Mn()}))})},u.openJSUrl=function(n,u,i,l){
if(!/^(void|\(void\))? ?(0|\(0\))?;?$/.test(n.slice(11).trim())){if(!i&&r.U){
if(0===l&&(r.U=o.indexFrame(r.U?r.U.s.Hn:r.oe,0)||r.U),o.safePost(r.U,{N:5,u:n,f:s.parseFallbackOptions(u)}))return
;r.U=null}var f=function(r){if(-1===r||t.Mn()){var l=e.on(n.slice(11));return t.jn(t.An.executeScript,{code:l
}).then(function(n){void 0===n&&i&&i(),y(void 0!==n,u,null)}),t.Mn()}s.runNextOnTabLoaded(u,null)}
;r.we<71?t.tabsUpdate({url:n},f):f(-1)}},x=function(n,e,i,l){var o,f,c,a=r.e.Ue
;return!(n.length<a.length+3||!n.startsWith(a))&&(l?(n=n.slice(a.length),o=l.incognito,
n.startsWith("#!image ")&&o&&(n="#!image incognito=1&"+n.slice(8).trim()),r.i=(f=[n,null,0])[1]=function(){
return clearTimeout(f[2]),r.i=null,f[0]},f[2]=setTimeout(function(){
f[0]="#!url vimium://error (vimium://show: sorry, the info has expired.)",f[2]=setTimeout(function(){
r.i===f[1]&&(r.i=null),f[0]="",f[1]=null},2e3)},1200),r.T=1,0!==e&&3!==e||o?o&&[0,3,-2,-1].indexOf(e)>=0?t.tabsCreate({
url:a,active:-2!==e},function(n){y(n,i,n)
}):(i.incognito=false,_([a],e,i,[l])):((c=r.we>=54&&!l.url.split("#",2)[1]?t.Bn.extension.getViews({tabId:l.id
}):[]).length>0&&c[0].location.href.startsWith(a)&&c[0].onhashchange?c[0].onhashchange():t.tabsUpdate(l.id,{url:a}),
s.runNextOnTabLoaded(i,null)),true):(t.getCurTab(function(r){if(!r||r.length<=0)return t.Mn();u.openShowPage(n,e,i,r[0])
}),true))},u.openShowPage=x,U=function(n){var e,l,o,f,c=r.z,a=c.urls;if(2!==c.$fmt){
if(1!==c.$fmt)for(e=0;e<a.length;e++)a[e]=i.er(a[e]+"");s.overrideOption("$fmt",2)}for(l of a)if(u.pu(l))return t.Mn()
;f=1===(o=u.parseReuse(c.reuse))||0===o||3===o?-1:o,r.z=null,_(a,f,c,n)},j=function(n,l,o,f){var c,a,v,p,m,w,g,y,I
;"string"!=typeof n||(n||9!==l?((a=s.fillOptionWithMask(n,r.z.mask,"value",["url","url_mask","value"],r.T)).ok&&(n=a.result,
a.useCount&&(r.T=1)),p=r.z.url_mark,null==(v=r.z.url_mask)&&null==p||(n=k(n,f,null!=v?v:p)),o&&(m=d.xr(r.z),
n=r.f(n,0,m)),
9!==l&&(w=(r.z.keyword||"")+"",n=(null!==(c=r.z.testUrl)&&void 0!==c?c:!w)?i.er(n,w,l):i.Qe(n.trim().split(e.hn),w)),
(g=r.z.goNext)&&n&&"string"==typeof n&&(n=r.f(n,8192),n=u.goToNextUrl(n,r.T,g)[1]),
n="string"==typeof n?i.Ye(n):n):n=r.newTabUrl_f),I=u.parseReuse((y=r.z).reuse),r.z=null,e.bn(),
"string"!=typeof n?n instanceof Promise?n.then(b.bind(0,l,y,f)):b(l,y,f,n):u.openShowPage(n,I,y)||(e.ln(n)?u.openJSUrl(n,y,null,I):u.pu(n)?s.runNextCmdBy(0,y):1===I?T(n,I,y.replace,null,{
u:n,p:y.prefix,q:u.parseOpenPageUrlOptions(y),f:s.parseFallbackOptions(y)
},f):0===I||3===I?h(y,I,n):y.replace?T(n,I,y.replace,y,null,f):f?_([n],I,y,f):t.getCurTab(_.bind(null,[n],I,y)))},
u.openUrlWithActions=j,z=function(n,f){var a,d,v,p,m,w,g,b,y,h,I,$,k
;if(null===f)return o.complainLimits("read clipboard"),void s.runNextCmd(0)
;if(!(f=f.trim()))return o.showHUD(c.Xn("noCopied")),void s.runNextCmd(0)
;if(v="string"==typeof(d=r.z.copied)&&d.includes("any"),("urls"===d||v)&&(a=f.split(/[\r\n]+/g)).length>1){
for(g of(p=[],m=v&&r.z.keyword?r.z.keyword+"":null,w=false,a))if(g=g.trim()){if(g=i.er(g,m,0),!(v||i.tr<=2)){p.length=0,
w=true;break}p.push(g)}if(p.length>1)return r.z=s.copyCmdOptions(e.fn(),r.z),r.z.urls=p,r.z.$fmt=1,
void(n&&n.length>0?U(n):t.getCurTab(U));if(w)return void(s.runNextCmd(0)||o.showHUD("The copied lines are not URLs"))}
i.ar.test(f)?f=f.slice(1,-1):(null!=(b=r.z.testUrl)?b:!r.z.keyword)&&(f=l.tu(f,b)),
(y=f.indexOf("://")+3)>3&&e.wn.test(f)&&(h=void 0,I=(f=/^ttps?:/i.test(f)?"h"+f:f).indexOf("/",y)+1||f.length,
$=f.slice(y,I),
f=(k=$.startsWith("0.0.0.0")?7:$.includes(":::")&&(h=/^(\[?::\]?):\d{2,5}$/.exec($))?h[1].length:0)?f.slice(0,y)+(k>6?"127.0.0.1":"[::1]")+f.slice(y+k):f),
u.openUrlWithActions(f,2,false,n)},u.goToNextUrl=function(n,u,r){var e=false
;return n=n.replace(/\$(?:\{(\d+)[,\/#@](\d*):(\d*)(:-?\d*)?\}|\$)/g,function(n,t,i,l,o){var f,c,a,s,d
;return"$$"===n?"$":(e=true,c=t&&parseInt(t)||1,a=i&&parseInt(i)||0,s=l&&parseInt(l)||0,
(d=o&&parseInt(o.slice(1))||1)<0&&(a=(f=[s,a])[0],s=f[1]),u*=d,c="absolute"!==r?c+u:u>0?a+u-1:u<0?s+u:c,
""+Math.max(a||1,Math.min(c,s?s-1:c)))}),[e,n]},L=function(n){var e,i
;if(r.z.urls)r.z.urls instanceof Array&&(n&&n.length>0?U(n):t.getCurTab(U));else{
if((null!=r.z.url_mask||null!=r.z.url_mark)&&!n)return t.Mn()||void t.getCurTab(u.openUrl)
;(e=r.z.url)?u.openUrlWithActions(e+"",3,true,n):r.z.copied?(i=r.h(d.xr(r.z)))instanceof Promise?i.then(z.bind(null,n)):z(n,i):u.openUrlWithActions(r.z.url_f||"",9,false,n)
}},u.openUrl=L,M=function(n,t){var f,c,a,d,v,p,w,g,b,y,h,I,$;if(e.sn(n),c=null!=t&&o.isNotVomnibarPage(t,true),
r.U=c?t:o.findCPort(t)||r.U,a=n.u,d=n.n&&s.parseFallbackOptions(n.n)||{},p=((v=n.o||e.fn()).k||"")+"",
w=null!==(f=v.t)&&void 0!==f?f:!p,g=v.s,y=(b=n.m||0)<64?-17&b:b,h=null!=n.f?n.f:43===y,d.group=!c||v.g,
d.incognito=null!=m(v.i)?v.i:43===y||null,
d.replace=v.m,d.position=v.p,d.reuse=null!=v.r?v.r:b?"window"===n.t?2:(16&b?-2:-1)+("last-window"===n.t?-4:0):n.r,
d.window=v.w,a)":"===a[0]&&!c&&/^:[bhtwWBHdso]\s/.test(a)&&(a=n.u=a.slice(2).trim()),I=a,a=w?l.ru(a,h):a,
a=r.f(a,c?h?1048576:524288:16384,g),$=void 0,h?a=($=a!==I)?i.er(a,null,-1):a:($=!!w||!c)?(a=w?l.tu(a,w):a,
a=i.er(a,p,c?-1:3)):(a=i.Qe(a.trim().split(e.hn),p,p&&"~"!==p?-1:0),
$=i.rr,a=i.rr?i.er(a,null,a.startsWith("vimium:")?3:0):a),
$&&(2!==i.tr&&1!==i.tr||null==n.h?3===i.tr&&a.startsWith("vimium:")&&!I.startsWith("vimium://")&&(a=i.er(a,null,i.rr||a.startsWith("vimium://run")?3:0)):a=(n.h?"https":"http")+a.slice("s"===a[4]?5:4)),
d.opener=c?false!==v.o:r.ge.vomnibarOptions.actions.includes("opener"),d.url_f=a;else{if(false===n.c)return
;d.copied=null==n.c||n.c,d.keyword=p,d.testUrl=v.t,d.sed=g}r.T=1,s.replaceCmdOptions(d),u.openUrl()},u.openUrlReq=M,
N=function(n,e){var l,o,f=function(e){var i,f,d,v=null!==(i=m(o.i))&&void 0!==i?i:2===r.ne&&null;return e=e||[],
null!==v&&(e=e.filter(function(n){return n.incognito===v})),o.g&&l.length>0&&(f=t.getGroupId(l[0]),
e=e.filter(function(n){return t.getGroupId(n)===f})),e.length>0?(d=e.filter(function(n){return n.windowId===r.te}),
void c(d.length>0?d:e)):(n.f&&s.runNextCmdBy(0,n.f)||(l.length<=0||o.w||2===r.ne&&!l[0].incognito?t.makeWindow({url:n.u,
type:w(o.w),incognito:2===r.ne&&!t.xn(n.u)},"",function(n){a(n&&n.tabs&&n.tabs.length>0?n.tabs[0]:null)
}):t.openMultiTabs({url:n.u,index:u.newTabIndex(l[0],o.p,false,true),openerTabId:o.o&&l[0]?l[0].id:void 0,
windowId:l[0].windowId,active:true},1,null,[null],o.g,l[0],a)),t.Mn())},c=function(u){var e,i,l,o=n.u
;n.p&&u.sort(function(n,u){return n.url.length-u.url.length}),(e=t.selectFrom(u)).url.length>u[0].url.length&&(e=u[0]),
!o.startsWith(r.e.Ee)||r.ie.get(e.id)||n.s?(i=r.be?e.url.replace(/^edge:/,"chrome:"):e.url,
l=r.be?o.replace(/^edge:/,"chrome:"):o,t.tabsUpdate(e.id,{url:i.startsWith(l)?void 0:o,active:true},a),
t.selectWndIfNeed(e)):(t.tabsCreate({url:o},a),t.An.remove(e.id))},a=function(u){
if(!u)return n.f&&s.runNextCmdBy(0,n.f),t.Mn();s.runNextOnTabLoaded(n.f||{},u,n.s&&function(){v.It.Bt(n,u)})
},d=i.Ye(n.u.split("#",1)[0]);u.pu(d,e)||((null==(o=n.q||(n.q={})).g||d.startsWith(r.e.Ee))&&(o.g=false),
o.m?T(n.u,null!=o.r&&u.parseReuse(o.r)||1,o.m,null,n):t.getCurTab(function(u){l=u;var r,e=w(o.w)||"normal"
;return!d.startsWith("file:")&&!d.startsWith("ftp")||d.includes(".",d.lastIndexOf("/")+1)||(r=d+(n.p?"/*":"/")),
n.p&&(d+="*"),t.An.query({url:r||d,windowType:e},function(n){return n&&n.length>0||!r?f(n):t.An.query({url:d,
windowType:e},f),t.Mn()}),t.Mn()}))},u.du=N});