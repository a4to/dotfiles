"use strict"
;__filename="background/others.js",define(["require","exports","./store","./browser","./utils","./settings","./i18n","./normalize_urls","./normalize_urls","./open_urls"],function(e,n,t,o,i,u,r,l,a,c){
Object.defineProperty(n,"__esModule",{value:true}),i=i,u=u;var s,f,m,d,g,p,v,b=t.ce.showActionIcon=function(e){
var n=o.Bn.browserAction;n?(t.H=e,o.import2("/background/action_icon.js").then(function(e){e.ei()}),
Promise.resolve(r.br("name")).then(function(t){e||(t+="\n\n"+r.br("noActiveState")),n.setTitle({title:t})
})):t.ce.showActionIcon=void 0};u.lo.then(function(){t.ge.showActionIcon?b(true):t.C=t.S}),setTimeout(function(){
new Promise(function(n,t){e(["/background/sync.js"],n,t)}).then(e=>e)},100),(function(){function e(){_&&(_.To=null),
y=D=_=T=null,M&&clearTimeout(M),w&&clearTimeout(w),j=C=F=M=w=0,h="",i.bn()}function n(){var t=Date.now()-j
;if(t>5e3||t<-5e3)return e();M=setTimeout(n,3e4)}function u(){var e,n;if(w=0,(e=_)&&!e.yo)return _=null,
e.To?((n=Date.now())<j&&(j=n-1e3),f(e.Po,e.To)):void 0}function s(e,n,u,r,l){var c,s,f,m,g,v,w,P,M,j,S,O,U,V,q,x,z
;if(e.To){for(_=null,c=n.length>0?n[0]:null,C=r,F=l,D=[],f=new Set,m=" ".concat(t.pe.s," ").includes(" type-letter "),
g=0,
v=u?0:1,w=n.length;g<w;g++)M=(P=n[g]).title,S=P.e,U="",V=null!=P.s,q=b&&!(u&&0===g)&&("tab"===S?P.s!==t.oe:"history"===S&&!V),
O=(O=i.tn(O=j=P.u,1)).startsWith("file")?a.$e(O):O.replace(/%20/g," "),
f.has(O)?O=":".concat(g+v," ").concat(O):f.add(O),q&&(U=" ~".concat(g+v,"~")),x={content:O,
description:U=(M||m?(M?M+" <dim>":"<dim>")+(m?"[".concat(P.e[0].toUpperCase(),"] "):"")+(M?"-</dim> <url>":"</dim><url>"):"<url>")+P.textSplit+"</url>"+(U&&"<dim>".concat(U,"</dim>"))
},q&&(x.deletable=true),(q||V)&&(y||(y=new Map),y.has(O)||y.set(O,{Do:S,Ai:V?P.s:null,or:j})),D.push(x);T=e.Po,
u?"search"===c.e?(s=((z=c.p)&&"<dim>".concat(i.mn(z)+d,"</dim>"))+"<url>".concat(c.textSplit,"</url>"),k=2,
(c=n[1])&&"math"===c.e&&(D[1].description="".concat(c.textSplit," = <url><match>").concat(c.t,"</match></url>"))):(k=3,
s=D[0].description):1!==k&&(s="<dim>".concat(p,"</dim><url>%s</url>"),k=1),u&&(h=n[0].u,
y&&D.length>0&&h!==D[0].content&&y.set(h,y.get(D[0].content)),D.shift()),s&&o.Bn.omnibox.setDefaultSuggestion({
description:s}),e.To(D),i.bn()}else _===e&&(_=null)}function f(e,o){var i,r,l,a;e=O(e),_&&(_.To=(i=e===_.Po)?o:null,
i)||(e!==T?1===C&&e.startsWith(T)?o([]):(_={To:o,Po:e,yo:false},w||(r=Date.now(),
(l=t.pe.t+j-r)>30&&l<3e3?w=setTimeout(u,l):(_.yo=true,M||(M=setTimeout(n,3e4)),j=r,y=D=null,h="",
a=C<2||!e.startsWith(T)?0:3===C?e.includes(" ")?0:8:F,t.j.yu(e,{o:"omni",t:a,r:S,c:P,f:1},s.bind(null,_))))):D&&o(D))}
function m(e,n,o){return e?":"===e[0]&&/^:([1-9]|1[0-2]) /.test(e)&&(e=e.slice(" "===e[2]?3:4)):e=l.er(""),
"file://"===e.slice(0,7).toLowerCase()&&(e=/\.(?:avif|bmp|gif|icon?|jpe?g|a?png|svg|tiff?|webp)$/i.test(e)?l.We("show image "+e,false,0):e),
null!=o?t.R[5]({s:o}):c.openUrlReq({u:e,r:"currentTab"===n?0:"newForegroundTab"===n?-1:-2})}
var d,g,p,v,b,T,h,_,w,y,P,D,M,j,k,C,F,S,O,U=o.Bn.omnibox;U&&(d=": ",g=false,p="Open: ",
b=!!(v=U.onDeleteSuggestion)&&"function"==typeof v.addListener,T=null,h="",_=null,w=0,y=null,P=128,D=null,M=0,k=0,C=0,
F=0,S=t.we<60?6:12,O=function(e){if(e=e.trim().replace(i.hn," "),t.ge.vomnibarOptions.actions.includes("icase")){
var n=/^:[WBH] /.test(e)?3:0;e=n?e.slice(0,n)+e.slice(n).toLowerCase():e.toLowerCase()}return e},
U.onInputStarted.addListener(function(){if(o.getCurWnd(false,function(e){var n=e&&e.width
;P=n?Math.floor((n-160/devicePixelRatio)/7.72):128}),g||(g=true,Promise.resolve(r.br("i18n")).then(function(){
"en"!==r.dr()&&Promise.resolve(r.Xn("colon")).then(function(e){d=e+r.Xn("NS")||d,p=r.Xn("OpenC")||p})})),M)return e()}),
U.onInputChanged.addListener(f),U.onInputEntered.addListener(function n(o,i){var r,l,a=_;if(a&&a.To){
if(a.To=n.bind(null,o,i),a.yo)return;return w&&clearTimeout(w),u()}return o=O(o),null===T&&o?t.j.yu(o,{o:"omni",t:0,r:3,
c:P,f:1},function(e,n){return n?m(e[0].u,i,e[0].s):m(o,i)
}):(h&&o===T&&(o=h),l=null==(r=null==y?void 0:y.get(o))?void 0:r.Ai,e(),m(r?r.or:o,i,l))}),b&&v.addListener(function(e){
var n=parseInt(e.slice(e.lastIndexOf("~",e.length-2)+1))-1,o=D&&D[n].content,i=o&&y?y.get(o):null,u=i&&i.Do;u?t.R[22]({
t:u,s:i.Ai,u:i.or
},null):console.log("Error: want to delete a suggestion but no related info found (may spend too long before deleting).")
}))})(),s=0,f=false,m=0,d=t.be?"edge:":"chrome:",g=t.be?"":d+"//newtab/",p=t.be?"":d+"//new-tab-page/",v=function(e){
0===e.frameId&&e.url.startsWith(d)&&s&(e.url.startsWith(g)||e.url.startsWith(p)?2:1)&&!m&&o._n(e.tabId)},o.In([{
origins:["chrome://*/*"]},t.we>79&&!t.be?{origins:["chrome://new-tab-page/*"]}:null],function e(n){
if(1&(s=(n[0]?1:0)+(n[1]?2:0))&&!t.ge.allBrowserUrls&&(s^=1),f!==!!s){var i=o.On();if(!i)return false
;i.onCommitted[(f=!f)?"addListener":"removeListener"](v)}m=m||s&&setTimeout(function(){s?o.An.query({url:d+"//*/*"
},function(e){for(var n of(m=0,e||[]))!t.ie.has(n.id)&&s&(n.url.startsWith(g)||n.url.startsWith(p)?2:1)&&o._n(n.id)
;return o.Mn()}):m=0},120),s&&!t.ce.allBrowserUrls&&(t.ce.allBrowserUrls=e.bind(null,n,false))}),
t.V&&t.V.then(function(e){var n=e&&e.reason,i="install"===n?"":"update"===n&&e.previousVersion||"none"
;"none"!==i&&setTimeout(function(){if(o.An.query({status:"complete"},function(e){var n,t=/^(file|ftps?|https?):/
;for(n of e)t.test(n.url)&&o._n(n.id)
}),console.log("%cVimium_ C%c has been %cinstalled%c with %o at %c%s%c.","color:red","color:auto","color:#0c85e9","color:auto",e,"color:#0c85e9",new Date(Date.now()-6e4*(new Date).getTimezoneOffset()).toJSON().slice(0,-5).replace("T"," "),"color:auto"),
t.e.Ge&&console.log("Sorry, but some commands of Vimium C require the permission to run in incognito mode."),i){
if(u.co("vomnibarPage"),!(parseFloat(t.e.Oe)<=parseFloat(i))&&(t.u?t.u(6e3):t.u=true,u.co("newTabUrl"),
t.ge.notifyUpdate)){var n="vimium_c-upgrade-notification"
;Promise.all([r.Xn("Upgrade"),r.Xn("upgradeMsg",[t.e.qe]),r.Xn("upgradeMsg2"),r.Xn("clickForMore")]).then(function(e){
var i,u={type:"basic",iconUrl:location.origin+"/icons/icon128.png",title:"Vimium C "+e[0],message:e[1]+e[2]+"\n\n"+e[3]}
;t.we<67&&(u.isClickable=true),t.we>=70&&(u.silent=true),(i=o.Bn.notifications)&&i.create(n,u,function(e){var u
;if(u=o.Mn())return u;n=e||n,i.onClicked.addListener(function e(n){n==n&&(i.clear(n),t.R[20]({u:l.er("vimium://release")
}),i.onClicked.removeListener(e))})})})}}else u.lo.then(function(){var e=function(){
t.B||t.b?++n<25&&setTimeout(e,200):t.R[20]({u:t.e.Ee+"#commands"})},n=0;setTimeout(e,200)})},500)}),
setTimeout(function(){globalThis.document.body.innerHTML="",i.bn()},1e3)});