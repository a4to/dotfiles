"use strict"
;__filename="background/page_handlers.js",define(["require","exports","./store","./utils","./browser","./normalize_urls","./parse_urls","./settings","./ports","./exclusions","./ui_css","./key_mappings","./run_commands","./tools","./open_urls","./frame_commands"],function(n,u,r,t,l,e,o,i,f,c,s,a,v,d,m,p){
var _,g,b,k;Object.defineProperty(u,"__esModule",{value:true}),u.onReq=void 0,i=i,c=c,_=[function(){
return[i.to,r.o,r.e.Je]},function(n){var u,t,l,e=r.b;if(e)return e.then(_[1].bind(null,n,null));for(t in u={},
i.to)(l=r.ge[t])!==i.to[t]&&(u[t]=l);return u},function(n){var u,t,l=n.key,e=n.val
;return e=null!==(u=null!=e?e:i.to[l])&&void 0!==u?u:null,i.ro(l,e),(t=r.ge[l])!==e?t:null},function(n){
var u=i.ao(n.key,n.val);return u!==n.val?u:null},function(n){i.so({N:6,d:n})},function(n){return r.ge[n.key]
},function(n){r.ie.has(n)||l._n(n)},function(){var n,u=a.mt;return!(!r.he.l||u||(n=Object.keys(r.X).join(""),
n+=r.W?Object.keys(r.W).join(""):"",!/[^ -\xff]/.test(n)))||(u?(function(n){
var u,r,t=n.length>1?n.length+" Errors:\n":"Error: ";for(r of n)u=0,t+=r[0].replace(/%([a-z])/g,function(n,t){return++u,
"c"===t?"":"s"===t||"d"===t?r[u]:JSON.stringify(r[u])}),u+1<r.length&&(t+=" ".concat(r.slice(u+1).map(function(n){
return"object"==typeof n&&n?JSON.stringify(n):n}).join(" "),".\n"));return t})(u):"")},function(n){
var u=f.indexFrame(n[1],0);return u&&u.s&&(u.s.Kn|=44),s.mergeCSS(n[0],-1)},function(n){n&&i.io("isHC_f",n.hc?"1":null),
s.ri(2)},function(n){return[e.er(n[0],null,n[1]),e.tr]},function(){d.Mt.Tr()},function(){var n=r.Q.get("?"),u="?"
;return n&&7===n.ft&&n.wt||r.Q.forEach(function(n,r){7===n.ft&&n.wt&&(u=u&&u.length<r.length?u:r)}),u},function(n){var u
;return[n=e.er(n,null,0),null!==(u=r.me.get(n))&&void 0!==u?u:null]},function(n){var u,r,t,l=new Map
;return o.uu("k:"+n,l),
null==(u=l.get("k"))?null:(r=e.er(u.or,null,-2),[!(t=e.tr>2),t?u.or:r.replace(/\s+/g,"%20")+(u.ou&&"k"!==u.ou?" "+u.ou:"")])
},function(n){m.du(n)},function(n){var u=null;return n.startsWith("vimium://")&&(u=r.p(n.slice(9),1,true)),
"string"==typeof(u=null!==u?u:e.er(n,null,-1))&&(u=o.tu(u,"whole"),u=e.Ye(u)),u},function(){return r.i&&r.i()
},function(n){return r.f(n[0],n[1])},function(n){return m.pu(n)},function(){
return Promise.all([l.jn(l.getCurTab),r.b]).then(function(n){
var u,e=n[0],o=e&&e[0]||null,f=o?o.id:r.oe,s=null!==(u=r.ie.get(f))&&void 0!==u?u:null,a=o?l.getTabUrl(o):s&&(s.Qn||s.Ln).s.or||"",v=!s||s.Ln.s.Cn&&!t.wn.test(s.Ln.s.or)?null:s.Ln.s,d=!(s||o&&a&&"loading"===o.status&&/^(ht|s?f)tp/.test(a)),m=k(s),p=!d&&!m,_=p?null:m||!a?m:a.startsWith(location.protocol)&&!a.startsWith(location.origin+"/")?new URL(a).host:null,g=_?r._e.get(_):null
;return p||null==g||true===g?_=null:s&&(s.Vn=-1),{ver:r.e.qe,runnable:p,url:a,tabId:f,
frameId:s&&(v||s.Qn)?(v||s.Qn.s).Cn:0,topUrl:v&&v.Cn&&s.Qn?s.Qn.s.or:null,frameUrl:v&&v.or,lock:s&&s.Dn?s.Dn.je:null,
status:v?v.je:0,unknownExt:_,exclusions:p?{rules:r.ge.exclusionRules,onlyFirst:r.ge.exclusionOnlyFirstMatch,
matchers:c.nt(null),defaults:i.to.exclusionRules}:null,os:r.o,reduceMotion:r.he.m}})},function(n){
var u,e,o=n[0],f=n[1],c=r.ge.extAllowList,s=c.split("\n")
;return s.indexOf(f)<0&&(u=s.indexOf("# "+f)+1||s.indexOf("#"+f)+1,s.splice(u?u-1:s.length,u?1:0,f),c=s.join("\n"),
i.ro("extAllowList",c)),(e=r.ie.get(o))&&(e.Vn=null),l.jn(l.Bn.tabs.get,o).then(function(n){var u=t.l(),r=function(){
return v.runNextOnTabLoaded({},n,u.yn),l.Bn.runtime.lastError};return n?l.Bn.tabs.reload(n.id,r):l.Bn.tabs.reload(r),
u.kn})},function(n){var u,t,l=n[1],e=n[2]
;return r.p("status/"+n[0],3),t=(u=f.indexFrame(l,e)||f.indexFrame(l,0))?r.ie.get(l).Dn:null,u&&!t&&r.R[8]({u:u.s.or
},u),[u?u.s.je:0,t?t.je:null]},function(n){return c.nt(n)[0]},function(n,u){return p.initHelp({f:true},u)},function(n){
var u,r,t,e=n.module,o=n.name,i=g[e];return g.hasOwnProperty(e)&&i.includes(o)?(r=n.args,t=(u=l.Bn[e])[o],
new Promise(function(n){r.push(function(u){var r=l.Mn();return n(r?[void 0,r]:[b(u),void 0]),r}),t.apply(u,r)
})):[void 0,{message:"refused"}]},function(n,u){return u.s.Hn},function(n){var u,l=t.fn();return n?(u=r.de.get(n),
l[n]=null!=u?u:null):r.de.forEach(function(n,u){l[u]=n}),l},function(n){var u=n.key,r=n.val;u.includes("|")&&i.io(u,r)
}],g={permissions:["contains","request","remove"],tabs:["update"]},b=function(n){return{
message:n&&n.message?n.message+"":JSON.stringify(n)}},u.onReq=function(n,u){return _[n.n](n.q,u)},k=function(n){
return n&&"string"==typeof n.Vn&&true!==r._e.get(n.Vn)?n.Vn:null}});