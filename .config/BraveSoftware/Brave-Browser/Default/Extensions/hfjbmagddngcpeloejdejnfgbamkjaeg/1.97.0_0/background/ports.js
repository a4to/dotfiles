"use strict"
;__filename="background/ports.js",define(["require","exports","./store","./utils","./browser","./exclusions","./i18n"],function(n,r,u,o,t,e,l){
var i,f,c,a,s,v,d,_,b,m,g,p,k;Object.defineProperty(r,"__esModule",{value:true}),
r.getParentFrame=r.complainNoSession=r.complainLimits=r.po=r.ensuredExitAllGrab=r.showHUD=r.safePost=r.isNotVomnibarPage=r.ensureInnerCSS=r.indexFrame=r.isExtIdAllowed=r.findCPort=r.Nn=r.Rn=r.OnConnect=r.sendResponse=void 0,
i=function(n,r){if(90!==n.H)u.R[n.H](n,r);else{var o=u.R[n.c](n.a,r,n.i);o!==r&&r.postMessage({N:4,m:n.i,r:o})}},
r.sendResponse=function(n,r,o){var t=u.ie.get(n.s.Hn);if(t&&t.wo.includes(n))try{n.postMessage({N:4,m:r,r:o})}catch(n){}
},r.OnConnect=function(n,r){var o,l,a,d,_,b,m,g,p;if(64&r)s(n,r);else{if(a=(l=(o=v(n)).or)===u.vomnibarPage_f,r>3||a){
if(999===r)return void(o.Hn>=0&&!o.Cn&&t.removeTempTab(o.Hn,n.sender.tab.windowId,o.or))
;if(16&r||a)return void c(n,r,a||l===u.e.ze)}
null!==(b=void 0!==(_=(d=o.Hn)>=0?u.ie.get(d):void(d=o.Hn=u.getNextFakeTabId()))?_.Dn:null)?(g=b.En,
p=2===(m=b.je)?3:1):(m=null===(g=e.Gn?e.Jn(l,o):null)?0:g?1:2,p=0),o.je=m,void 0!==_&&(p|=4&_.Kn,32&r&&(p|=128,
_.Kn|=128),o.Kn=p),4&r?(o.Kn|=8&r,n.postMessage({N:1,p:g,f:3&p}),n.postMessage({N:6,d:u.he})):n.postMessage({N:0,f:p,
c:u.he,p:g,m:u.W,t:u.K,k:u.X}),n.onDisconnect.addListener(f),n.onMessage.addListener(i),
void 0!==_?(2&r&&(u.H&&_.Ln.s.je!==m&&u.C(d,m),_.Ln=n),1&r&&!_.Qn&&(_.Qn=n),_.wo.push(n)):(u.ie.set(d,{Ln:n,
Qn:1&r?n:null,wo:[n],Dn:null,Kn:0}),0!==m&&u.H&&u.C(d,m))}},f=function(n){var r,o,t=n.s.Hn,e=u.ie.get(t)
;e&&(r=(o=e.wo).lastIndexOf(n),n.s.Cn?(r===o.length-1?--o.length:r>=0&&o.splice(r,1),
o.length?n===e.Ln&&(e.Ln=o[0]):u.ie.delete(t)):r>=0&&u.ie.delete(t))},c=function(n,r,e){if(r>15){
if(e)return n.s.Hn<0&&(n.s.Hn=4&r?u.getNextFakeTabId():u.U?u.U.s.Hn:u.oe),n.s.Kn|=256,u.ue.push(n),
n.onDisconnect.addListener(a),n.onMessage.addListener(i),void(4&r||n.postMessage({N:42,l:u.pe,s:o.n(false)}))
}else n.s.Hn<0||u.we<50||0===n.s.Cn||t.An.executeScript(n.s.Hn,{file:u.e.Fe,frameId:n.s.Cn,runAt:"document_start"},t.Mn)
;n.disconnect()},a=function(n){var r=u.ue,o=r.lastIndexOf(n);o===r.length-1?--r.length:o>=0&&r.splice(o,1)},
s=function(n,r){32&r?n.disconnect():(n.s=false,n.onMessage.addListener(i))},v=function(n){var r=n.sender,u=r.tab
;return r.tab=null,n.s={Cn:r.frameId||0,je:0,Kn:0,Sn:null!=u&&u.incognito,Hn:null!=u?u.id:-3,or:r.url}},
d=function(n,o,l){var i
;return(n=n||(null===(i=u.ie.get(u.oe))||void 0===i?void 0:i.Qn))&&e.Gn&&(o||e.Un)?n.s.or:new Promise(function(o){
var e=u.we>48&&n&&n.s.Cn?t.On():null;n?(e?e.getFrame:t.tabsGet)(e?{tabId:n.s.Hn,frameId:n.s.Cn}:n.s.Hn,function(u){
var i=u?u.url:"";return!i&&e&&(l.N=3,r.safePost(n,l)),o(i),t.Mn()}):t.getCurTab(function(n){
return o(n&&n.length?t.getTabUrl(n[0]):""),t.Mn()})})},r.Rn=d,_=function(n,o){var t,e
;if(u.U=u.U||(null===(t=u.ie.get(u.oe))||void 0===t?void 0:t.Qn),
"string"!=typeof(e=r.Rn(u.U,o,n)))return e.then(function(r){return n.u=r,r&&u.R[n.H](n,u.U),r});n.u=e,u.R[n.H](n,u.U)},
r.Nn=_,r.findCPort=function(n){var r=u.ie.get(n?n.s.Hn:u.oe);return r?r.Ln:null},r.isExtIdAllowed=function(n){
var r,o,t=null!=n.id?n.id:"unknown_sender",e=n.url,l=n.tab,i=u._e,f=i.get(t)
;return true!==f&&l&&(o=(r=u.ie.get(l.id))?r.Vn:null,r&&(null==o||o!==t&&"string"==typeof o)&&(r.Vn=null==o?t:2)),
null!=f?f:e===u.vomnibarPage_f||(console.log("%cReceive_ message from an extension/sender not in the allow list: %c%s","background-color:#fffbe5","background-color:#fffbe5;color:red",t),
i.set(t,false),false)},r.indexFrame=function(n,r){var o,t=u.ie.get(n);for(o of t?t.wo:[])if(o.s.Cn===r)return o
;return null},r.ensureInnerCSS=function(n){if(8&n.Kn)return null;var r=u.ie.get(n.Hn);return r&&(r.Kn|=4),n.Kn|=12,u.q},
r.isNotVomnibarPage=function(n,r){var u=n.s,o=u.Kn
;return!(256&o)&&(r||512&o||(console.warn("Receive a request from %can unsafe source page%c (should be vomnibar) :\n %s @ tab %o","color:red","color:auto",u.or.slice(0,128),u.Hn),
u.Kn|=512),true)},r.safePost=function(n,r){try{return n.postMessage(r),1}catch(n){return 0}},b=function(n,u){
r.showHUD(u,n)},m=function(n,o){
"string"==typeof n?(o&&(n.startsWith(u.e.xe+"-")&&n.includes("://")&&(n=n.slice(n.indexOf("/",n.indexOf("/")+2)+1)||n),
n=n.length>41?n.slice(0,41)+"\u2026":n+"."),u.U&&!r.safePost(u.U,{N:12,H:r.ensureInnerCSS(u.U.s),k:o?n?20:o:1,t:n
})&&(u.U=null)):n.then(b.bind(null,o))},r.showHUD=m,r.ensuredExitAllGrab=function(n){var r,u={N:8}
;for(r of n.wo)4&r.s.Kn||(r.s.Kn|=4,r.postMessage(u));n.Kn|=4},r.po=function(n,r){
var t,e=o.gn(u.ie),l=u.oe,i=function(r){var o=u.ie.get(r),t=0;return null!=o&&(t=Math.min(o.wo.length,8),n(o)),t}
;e.length<50?((t=e.indexOf(l))>=0&&(e.splice(t,1),n(u.ie.get(l))),o.c(e,i,r)):e.forEach(i)},g=function(n){
Promise.resolve(n).then(function(n){r.showHUD(l.Xn("notAllowA",[n]))})},r.complainLimits=g,p=function(){
r.complainLimits("control tab sessions")},r.complainNoSession=p,k=function(n,u,o){
return u&&t.On()?1===o&&true?t.jn(t.On().getFrame,{tabId:n,frameId:u}).then(function(u){var o=u?u.parentFrameId:0
;return o>0?r.indexFrame(n,o):null}):t.jn(t.On().getAllFrames,{tabId:n}).then(function(t){var e,l=false,i=u
;if(!t)return null;do{for(e of(l=false,t))if(e.frameId===i){l=(i=e.parentFrameId)>0;break}}while(l&&0<--o)
;return i>0&&i!==u?r.indexFrame(n,i):null}):Promise.resolve(null)},r.getParentFrame=k});