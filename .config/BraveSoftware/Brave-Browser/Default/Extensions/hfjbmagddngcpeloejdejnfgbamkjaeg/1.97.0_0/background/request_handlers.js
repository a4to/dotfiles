"use strict"
;__filename="background/request_handlers.js",define(["require","exports","./store","./utils","./browser","./parse_urls","./settings","./ports","./exclusions","./ui_css","./i18n","./key_mappings","./run_commands","./run_keys","./open_urls","./frame_commands","./tools"],function(n,u,t,r,e,o,i,f,l,c,s,a,v,d,m,b,p){
var g,y,_,j,k,N;Object.defineProperty(u,"__esModule",{value:true}),r=r,i=i,g=-1,t.R=[function(n,u){var r,e,o=n.k,l=i.oo
;if(!(o>=0&&o<l.length))return t.U=u,f.complainLimits(s.Xn("notModify",[o]));e=t.b,
t.ge[r=l[o]]!==n.v&&(e?e.then(function(){i.ro(r,n.v)}):i.ro(r,n.v))},function(n,u){var t="object"==typeof n
;return p.Ct.$t(u.s.Sn,t?n.q:"",t?1:n)},function(n,u){var t=o.fu(n);if(null==n.i)return t;u.postMessage({N:44,i:n.i,s:t
})},function(n,u){var i=n.u,l=n.e,c=o.iu(n)
;r.bn(),n.e=c,null==c.p?(t.U=u,f.showHUD(c.u)):l||i!==c.u?!u||"file://"===c.u.slice(0,7).toLowerCase()&&"file://"!==i.slice(0,7).toLowerCase()?e.tabsUpdate({
url:c.u}):v.sendFgCmd(0,false,{r:1,u:c.u}):(t.U=u,f.showHUD("Here is just root"),n.e={p:null,u:"(just root)"})
},function(n,u){var r,e,i=o.fu(n);if(!i||!i.k)return t.U=u,f.showHUD(s.Xn("noEngineFound")),
void(n.n&&v.runNextCmdBy(0,n.n));e=n.o||{},r=n.t.trim()&&t.f(n.t.trim(),524288,e.s).trim()||(n.c?t.h(e.s):""),
Promise.resolve(r).then(function(r){
var o=null===r?"It's not allowed to read clipboard":(r=r.trim())?"":s.Xn("noSelOrCopied");if(o)return t.U=u,
f.showHUD(o),void(n.n&&v.runNextCmdBy(0,n.n));e.k=null==e.k?i.k:e.k,t.R[6]({u:r,o:e,r:0,
n:v.parseFallbackOptions(n.n)||{}},u)})},function(n,u){var r,o=n.s,i=false!==n.a;t.U=f.findCPort(u),
"number"!=typeof o?e.Tn()?(e.Tn().restore(o[1],function(){var n=e.Mn();return n&&f.showHUD(s.Xn("noSessionItem")),n}),
i||((r=u.s.Hn)>=0||(r=t.oe),r>=0&&e.selectTab(r))):f.complainNoSession():e.selectTab(o,function(n){
return e.Mn()?f.showHUD(s.Xn("noTabItem")):e.selectWnd(n),e.Mn()})},m.openUrlReq,function(n,u){var r,e,o,i
;(e=t.ie.get(r=u.s.Hn))?u!==(i=e.Ln)&&(e.Ln=u,t.H&&(o=u.s.je)!==i.s.je&&t.C(r,o)):t.H&&t.C(r,u.s.je)},function(n,u){
var r,e,o,i,c,s=u;if((s||(s=f.indexFrame(n.tabId,n.frameId)))&&(e=(r=s.s).or,!(o=t.ie.get(r.Hn))||!o.Dn)){
if(i=l.Gn?l.Jn(r.or=u?n.u:n.url,r):null,
r.je!==(c=null===i?0:i?1:2))r.je=c,t.H&&o.Ln===s&&t.C(r.Hn,c);else if(!i||i===l.Jn(e,r))return;s.postMessage({N:1,p:i,
f:0})}},function(n,u){var r,e=n.t||0;t.U=u,t.T=e||t.T>0?1:-1,t.F=n.k,v.replaceCmdOptions(n.f||{}),
2!==e?1===e?b.parentFrame():b.nextFrame():(r=t.ie.get(u.s.Hn))?b.focusFrame(r.Ln,r.wo.length<=2,1,t.z):f.safePost(u,{
N:45,l:t.F})},function(n,u){var r,e,o,i=t.ie.get(u.s.Hn);if(i&&(u.s.Kn|=4,i.Kn|=4,!(i.wo.length<2)))for(e of(r={N:8},
i.wo))o=e.s.Kn,e.s.Kn|=4,4&o||e.postMessage(r)},function(n,u,r){var o,i,l=u.s.Hn,c=t.ie.get(l),s=n.u
;if(!c||c.wo.length<2)return false;for(i of c.wo)if(i.s.or===s){if(o){o=null;break}o=i}return o?(t.F=n.k,j(n,u,o,1),
true):!!e.On()&&(e.On().getAllFrames({tabId:u.s.Hn},function(e){var o,i,c=0,s=u.s.Cn;for(o of e)if(o.parentFrameId===s){
if(c){c=0;break}c=o.frameId}(i=c&&f.indexFrame(l,c))&&(t.F=n.k,j(n,u,i,1)),f.sendResponse(u,r,!!i)}),u)},function(n,u){
b.initHelp(n,u)},function(n,u){t.ie.get(u.s.Hn).Kn|=4,u.s.Kn|=12,u.postMessage({N:12,H:t.q})},function(n,u){
var e,i,f,l,c=n.i;if(t.F=0,null!=n.u)i=n.t,l=n.u,l=(f=(e=n.m)>=40&&e<=64)?o.ru(l,true):l,l=t.f(l,f?1048576:524288),
v.replaceCmdOptions({url:l,newtab:null!=i?!!i:!f,keyword:n.o.k}),k(n.f),t.T=1;else{if(true!==n.r)return
;if(null==t.z||"omni"!==t.z.k){if(c)return;t.z=r.fn(),t.T=1}else if(c&&t.z.v===t.e.ze)return}t.U=u,b.showVomnibar(c)
},function(n,u){f.isNotVomnibarPage(u,false)||t.j.yu(n.q,n,_.bind(u,0|n.i))},function(n,u){
var e,i=n.u||n.s,l=null!=n.s&&n.m||0,c=n.e,s=l>=40&&l<=64&&(!c||false!==c.r)
;if(n.d)if("string"!=typeof i)for(e=i.length;0<=--e;)s&&(i[e]=o.ru(i[e]+"")),i[e]=r.rn(i[e]+"");else s&&(i=o.ru(i)),
i=r.rn(i);else"string"==typeof i&&i.length<4&&i.trim()&&" "===i[0]&&(i="");i=i&&t.w(i,n.j,c),t.U=u,
i=n.s&&"object"==typeof n.s?"[".concat(n.s.length,"] ")+n.s.slice(-1)[0]:i,
f.showHUD(n.d?i.replace(/%[0-7][\dA-Fa-f]/g,decodeURIComponent):i,n.u?14:15)},function(n,u){
var e,o,i,f,l,c,s=null!=u?u.s:null;null===s||4&s.Kn||(s.Kn|=4,(e=t.ie.get(s.Hn))&&(e.Kn|=4)),i=1,
null!=(f=/^\d+|^-\d*/.exec(o=n.k))&&(o=o.slice((l=f[0]).length),i="-"!==l?parseInt(l,10)||1:-1),
(c=t.Q.get(o))||(f=o.match(a.bt),i=1,c=t.Q.get(o=f[f.length-1])),r.bn(),c&&(36===c.ft&&c.wt&&d.yt(c),n.e&&(t.L={
element:r.g(n.e)}),v.executeCommand(c,i,n.l,u,0,null))},v.waitAndRunKeyReq,function(n,u){switch(t.U=u,n.a){case 1:
return p.It.xt(n,u);case 0:return p.It.zt(n,u);case 2:return p.It.Et(n.u);default:return}
},m.du,v.onConfirmResponse,function(n,u){
var r,o,i=n.t,l=n.s,c=n.u,a="history"===i&&null!=l?"session":i,v="tab"===a?a:a+" item",d=function(n){
Promise.resolve(s.Xn("sugs")).then(function(u){f.showHUD(s.Xn(n?"delSug":"notDelSug",[s.vr(u,a[0])||v]))})}
;t.U=f.findCPort(u),
"tab"===a&&t.oe===l?f.showHUD(s.Xn("notRemoveCur")):"session"!==a?t.j._u("tab"===a?l:c,a,d):(null===(r=e.Tn())||void 0===r?void 0:r.forgetClosedTab)&&(o=l,
e.Tn().forgetClosedTab(o[0],o[1]).then(function(){return 1},t.S).then(d))},b.openImgReq,function(n,u){t.U=null,
m.openJSUrl(n.u,{},function(){t.U=u,f.showHUD(s.Xn("jsFail"))})},function(n,u){var r
;2!==n.c&&4!==n.c?j(n,u,(null===(r=t.ie.get(u.s.Hn))||void 0===r?void 0:r.Qn)||null,n.f):f.getParentFrame(u.s.Hn,u.s.Cn,1).then(function(r){
var e;j(n,u,r||(null===(e=t.ie.get(u.s.Hn))||void 0===e?void 0:e.Qn)||null,n.f)})},c.ii,function(n,u){
v.replaceCmdOptions({active:true,returnToViewport:true}),t.U=u,t.T=1,b.performFind()},b.framesGoBack,function(){
return s.cr&&s.cr(),s.gr},function(n,u){u.s.Kn|=8},function(n,u){v.replaceCmdOptions({mode:n.c?"caret":"",start:true}),
k(n.f),t.U=u,t.T=1,b.enterVisualMode()},function(n){if(performance.now()-n.r.n<500){var u=n.r.c;u.element=r.g(n.e),
d.runKeyWithCond(u)}},function(n,u){n.u=t.f(o.ru(n.u,true),1048576),e.downloadFile(n.u,n.f,n.r,n.m<42?function(r){
r||t.R[23]({m:36,f:n.f,u:n.u},u)}:null)},function(n,u,t){return setTimeout(function(){f.sendResponse(u,t,9)},n),u
},function(n){var u=n.v,t=u!==!!u;f.showHUD(s.Xn(t?"useVal":u?"turnOn":"turnOff",[n.k,t?JSON.stringify(u):""]))
},function(n,u){var r=u.s.Hn,e=t.ie.get(r>=0?r:t.oe);t.R[17](n,e?e.Ln:null)},function(n,u,t){
return!(false!==u.s&&!u.s.or.startsWith(location.origin+"/"))&&(N(n.q,n.i,u).then(function(n){u.postMessage(t?{N:4,m:t,
r:n}:n)}),u)}],_=function(n,u,e,o,i,l,c,s){var a,v,d,m,b,p=this.s.or,y=2===n?2:0
;if(1===n&&t.we>=58)if(p=p.slice(0,p.indexOf("/",p.indexOf("://")+3)+1),
null!=(m=-1!==g?null===(a=t.ie.get(g))||void 0===a?void 0:a.Qn:null)&&(m.s.or.startsWith(p)?y=1:g=-1),
y);else for(b of t.ie.values())if((d=(v=b.Qn)&&v.s)&&d.or.startsWith(p)){y=1,g=d.Hn;break}f.safePost(this,{N:43,a:e,c:s,
i:y,l:u,m:o,r:c,s:i,t:l}),r.bn()},j=function(n,u,r,e){r&&2!==r.s.je?r.postMessage({N:7,
H:e||4!==n.c?f.ensureInnerCSS(u.s):null,m:e?4:0,k:e?t.F:0,f:{},c:n.c,n:n.n||0,a:n.a}):(n.a.$forced=1,
v.portSendFgCmd(u,n.c,false,n.a,n.n||0))},k=function(n){n&&("string"==typeof n&&(n=d.parseEmbeddedOptions(n)),
n&&"object"==typeof n&&Object.assign(t.z,r.sn(n)))},N=function(u,t,r){return y||(y=new Promise(function(u,t){
n(["/background/sync.js"],u,t)}).then(n=>n).then(function(){return i.lo}).then(function(){
return e.import2("/background/page_handlers.js")})),y.then(function(n){return Promise.all(u.map(function(u){
return n.onReq(u,r)}))}).then(function(n){return{i:t,a:n.map(function(n){return void 0!==n?n:null})}})},
globalThis.window&&(globalThis.window.onPagesReq=function(n){return N(n.q,n.i,null)})});