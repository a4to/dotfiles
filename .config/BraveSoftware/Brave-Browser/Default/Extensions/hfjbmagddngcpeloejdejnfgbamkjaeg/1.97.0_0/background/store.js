"use strict";__filename="background/store.js",define(["require","exports"],function(e,n){var l,t,o,r
;Object.defineProperty(n,"__esModule",{value:true
}),n.e=n.o=n.r=n.u=n.i=n.p=n.f=n.h=n.w=n.b=n.M=n.C=n.i=n.u=n.p=n.f=n.h=n.w=n.b=n.M=n.C=n.getNextFakeTabId=n.j=n.S=n.P=n.V=n.I=n.N=n.R=n.x=n.y=n.B=n.D=n.G=n.H=n.O=n.q=n.A=n.E=n.vomnibarPage_f=n.newTabUrl_f=n.J=n.L=n.L=n.T=n.U=n.z=n.z=n.F=n.K=n.Q=n.W=n.X=n.Y=n.findBookmark=n.Z=n.$=n.ee=n.ne=n.le=n.te=n.oe=n.P=n.I=n.N=n.T=n.U=n.F=n.K=n.Q=n.W=n.X=n.findBookmark=n.re=n.ae=n.se=n.Z=n.$=n.ne=n.le=n.te=n.oe=n.ee=n.ue=n.ie=n.ce=n.R=n.B=n.D=n._e=n.me=n.Y=n.y=n.G=n.H=n.O=n.q=n.A=n.E=n.pe=n.fe=n.he=n.vomnibarPage_f=n.newTabUrl_f=n.de=n.ge=n.x=n.J=n.V=n.o=n.r=n.we=n.be=n.OnSafari=n.OnEdge=n.OnFirefox=n.OnChrome=n.IsLimited=n.Me=void 0,
n.Me=1,n.IsLimited=!!0,n.OnChrome=!0,n.OnFirefox=!!0,n.OnEdge=!!0,n.OnSafari=!!0,l=navigator.userAgentData,
n.be=l?!!l.brands.find(function(e){return e.brand.includes("Edge")||e.brand.includes("Microsoft")
}):matchMedia("(-ms-high-contrast)").matches,n.we=l?(t=l.brands.find(function(e){return e.brand.includes("Chromium")
}))?t.version:90:(r=navigator.userAgent.match(/\bChrom(?:e|ium)\/(\d+)/))?75==+r[1]&&matchMedia("(prefers-color-scheme)").matches?81:0|r[1]:998,
n.r=999,n.o=2,n.J=false,n.x=false,n.ge={},n.de=new Map,n.newTabUrl_f="",n.vomnibarPage_f="",n.he={v:n.we,d:"",g:false,
m:false},n.fe={map:new Map,rules:[],keywords:null},n.pe={v:n.we,a:0,c:"",l:"",k:null,n:0,s:"",t:0},n.E=false,n.H=false,
n.me=new Map,n._e=new Map,n.D=0,n.ce={},n.ie=new Map,n.ue=[];n.ee=new Map,n.oe=-1,n.te=-1,n.le=-1,n.ne=1,n.$=null,
n.Z=null,n.se={ve:[],Ce:[],je:0,Se:0,ke:false},n.ae={Pe:null,Ve:new Map,Ie:0,Ne:0,Re:0},n.re=new Map,n.X=null,n.W=null,
n.K=0,n.F=0,n.U=null,n.T=1,n.z=null,n.L=null,n.S=function(){},n.j={},o=-4,n.getNextFakeTabId=function(){return o--},
n.C=n.S,n.M=n.S,n.b=null,n.w=function(){return""},n.h=function(){return""},n.f=function(e){return e},n.p=function(){
return null},n.u=null,n.i=null,n.e={xe:"chrome",ye:true,Be:0,
De:n.be?/^https:\/\/(ntp|www)\.msn\.\w+\/(edge|spartan)\/ntp\b/:"chrome-search://local-ntp/local-ntp.html",Ge:false,
He:null,Oe:"",qe:"",GitVer:"1b10b3d",Ae:"/lib/injector.js",HelpDialogJS:"/background/help_dialog.js",
Ee:"pages/options.html",Je:"browser",Le:"https://github.com/gdh1995/vimium-c",Te:null,Ue:"pages/show.html",ze:"",
Fe:"/front/vomnibar.js",Ke:""}});