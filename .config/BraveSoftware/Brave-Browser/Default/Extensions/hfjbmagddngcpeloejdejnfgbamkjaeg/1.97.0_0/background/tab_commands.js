"use strict"
;__filename="background/tab_commands.js",define(["require","exports","./store","./utils","./browser","./normalize_urls","./parse_urls","./ports","./i18n","./run_commands","./clipboard","./open_urls","./frame_commands","./filter_tabs","./tools"],function(n,e,r,u,t,i,o,f,l,c,a,d,s,v,w){
var m,p,b,I,g,_,x,T,h;Object.defineProperty(e,"__esModule",{value:true
}),e.Ft=e.toggleTabUrl=e.togglePinTab=e.toggleMuteTab=e.removeTab=e.reloadTab=e.moveTabToNextWindow=e.moveTabToNewWindow=e.joinTabs=e.copyWindowInfo=void 0,
u=u,m=Math.abs,p=function(){r.U&&s.focusFrame(r.U,false,0)},b=function(n){
return r.z.end?null:null!=r.z.position?d.newTabIndex(n,r.z.position,false,false):null!=r.z.rightInOld?n.index+(r.z.rightInOld?0:1):n.index+(false!==r.z.right?1:0)
},e.copyWindowInfo=function(n){
var e,i=r.z.filter,o=!!(r.z.decoded||r.z.decode),c=r.z.type,d="tab"===c&&(m(r.T)>1||!!i),s=a.xr(r.z)
;if("frame"===c&&r.U)return e=void 0,128&r.U.s.Kn?(r.U.postMessage({N:3,H:16,d:o,e:s}),e=1):e=f.Nn({H:16,u:"",d:o,e:s}),
void(1!==e&&(e&&e instanceof Promise?e.then(function(){n(1)}):n(1)));t.An.query("browser"===c?{windowType:"normal"}:{
active:"window"!==c&&!d||void 0,currentWindow:true},function(e){var a,w,m,p,b,I,g,_,x,T,h
;if(!c||"title"===c||"frame"===c||"url"===c)return r.R[16]({u:"title"===c?e[0].title:t.getTabUrl(e[0]),d:o,e:s},r.U),
void n(1);a=r.U?r.U.s.Sn:2===r.ne,m=""+((w=r.z.format)||"${title}: ${url}"),b="json"===(p=r.z.join)&&!w,
d?(I=e.length<2?0:t.selectIndexFrom(e),g=v.getTabRange(I,e.length),e=e.slice(g[0],g[1])):e=e.filter(function(n){
return n.incognito===a}),i&&(_=r.U?r.U.s.Hn:r.oe,x=e.find(function(n){return n.id===_}),e=v.mu(x,e,i)),
e.length?("browser"===c&&e.sort(function(n,e){return n.windowId-e.windowId||n.index-e.index}),T=e.map(function(n){
return b?{title:n.title,url:o?u.rn(t.getTabUrl(n)):t.getTabUrl(n)}:m.replace(/\$\{([^}]+)}/g,function(e,r){
return r.split("||").reduce(function(e,r){var i
;return e||(o&&"url"===r?u.rn(t.getTabUrl(n)):"__proto__"!==r&&((i=n[r])&&"object"==typeof i?JSON.stringify(i):i||""))
},"")})}),h=r.w(T,p,s),f.showHUD("tab"===c&&e.length<2?h:l.Xn("copiedWndInfo"),15),n(1)):n(0)})},e.joinTabs=function(n){
var e,u=null!=r.z.order?r.z.order:r.z.sort,i=r.z.windows,o="current"===i;e=function(e){var f,l,c,a,d;f=2===r.ne,
e=o?e:e.filter(function(n){return n.incognito===f}),l=o?e:e.filter(function(n){return n.id===r.te}),
o||l.length?(c=function(i){var f,l,c,a,d,s,w,p,b,I,g=[],_=function(n){g.push(n)};if(e.sort(function(n,e){
return n.id-e.id}).forEach(function(n){n.tabs.forEach(_)}),g.length)if(f=r.z.filter,l=i?i.id:r.te,c=g.find(function(n){
return n.id===r.oe})||(i?t.selectFrom(i.tabs):g[0]),o&&m(r.T)>1&&g.length>1&&(a=g.findIndex(function(n){
return n.id===c.id}),d=v.getTabRange(a,g.length),g=g.slice(d[0],d[1])),f&&(g=v.mu(c,g,f,s={}),f=s.known?f:null),
g.length){for(I of(g=u?v.hu(g,u):g,p="before"===(w=r.z.position)||(w+"").startsWith("prev"),
f&&i?w&&"string"==typeof w&&"keep"!==w?"begin"===w||"start"===w?b=i.tabs.filter(function(n){return n.pinned
}).length:"end"!==w?(g.includes(c)&&g.splice(g.indexOf(c),1),p?g.push(c):g.unshift(c),
b=Math.max(0,i.tabs.findIndex(function(n){return n.id===r.oe})-g.filter(function(n){
return n.windowId===l&&n.index<c.index}).length)):b=i.tabs.length:b=g.reduce(function(n,e){
return e.windowId===l?Math.min(e.index,n):n},g.length):b=i?i.tabs.length:0,g))t.An.move(I.id,I.windowId!==l?{windowId:l,
index:b++}:{index:b++});for(I of g)I.pinned&&I.windowId!==l&&t.tabsUpdate(I.id,{pinned:true});n(1)}else n(0);else n(0)},
(a=l.length?l[0]:null)&&"popup"===a.type&&a.tabs.length?(d=t.selectFrom(a.tabs).id,a.tabs=a.tabs.filter(function(n){
return n.id!==d}),t.makeWindow({tabId:d,incognito:a.incognito},a.state,function(n){n&&(r.te=n.id,
n.tabs[0]&&(r.oe=n.tabs[0].id)),c(n)})):(e=o||!a||"all"===i?e:e.filter(function(n){return n.id!==a.id}),c(a))):n(0)},
o?t.getCurWnd(true,function(n){return n?e([n]):t.Mn()}):(r.T=1,t.$n.getAll({populate:true,windowTypes:["normal","popup"]
},e))},I=function(n){var u="hasIncog",i=!!r.z.all,o=function(e){
var u,a,d,s,w,I,g,_,x,T,h,y=e.tabs,k=y.length,j=false!==r.z.focused,M=t.selectIndexFrom(y),O=y[M]
;if(!i&&k<=1&&(!k||0===O.index&&m(r.T)>1))n(0);else{if(i){
for(d of y)if(null!=t.getGroupId(d))return f.showHUD("Can not keep groups info during this command"),void n(0);a=[0,k]
}else a=1===k?[0,1]:v.getTabRange(M,k);if(s=r.z.filter,w=y.slice(a[0],a[1]),(w=s?v.mu(O,w,s):w).length){if(!i){
if((I=w.length)>=k&&k>1)return n(0),void f.showHUD(l.Xn("moveAllTabs"))
;if(I>30&&c.vu())return void c.cu("moveTabToNewWindow",I).then(o.bind(null,e))
;if(1===k&&0===O.index&&1===m(r.T))return void t.jn(t.An.query,{windowId:e.id,index:1}).then(function(r){
if(!r||!r.length)return n(0),void f.showHUD(l.Xn("moveAllTabs"));e.tabs=[e.tabs[0],r[0]],o(e)})}g=O.incognito,
_=w.includes(O)?O:w[0],x=(null!==(u=b(O))&&void 0!==u?u:3e4)<=O.index,T={tabId:_.id,incognito:g,focused:j},
h="normal"===e.type?e.state:"",v.ku(w[x?w.length-1:0],x,y).then(function(u){j||u&&t.selectTab(u.id),
t.makeWindow(T,h,function(i){var o,f,l,c,a,d,s,v;if(i){for(v of(p(),j&&u&&t.selectTab(u.id),o=w.indexOf(_),
f=w.slice(0,o),l=w.slice(o+1),e.incognito&&r.we<52&&(f=f.filter(c=function(n){return n.incognito===g}),l=l.filter(c)),
d=l.length,s=function(n){return n.id},(a=f.length)&&(t.An.move(f.map(s),{index:0,windowId:i.id},t.Mn),
a>1&&t.An.move(w[o].id,{index:a})),d&&t.An.move(l.map(s),{index:a+1,windowId:i.id},t.Mn),
w))v.pinned&&t.tabsUpdate(v.id,{pinned:true});n(1)}else n(0)})})}else n(0)}},a=function(i){
var o,a,s,v=t.selectFrom(i.tabs);if(i.incognito&&v.incognito)return n(0),f.showHUD(l.Xn(u));if(o=v.id,a={incognito:true
},s=t.getTabUrl(v),v.incognito);else{if(i.incognito)return t.xn(s)?(n(0),f.showHUD(l.Xn(u))):e.Ft(v)
;if(t.xn(s))return n(0),f.complainLimits(l.Xn("openIncog"));a.url=s}i.tabs=null,t.$n.getAll(function(e){
var u,f,l=false!==r.z.focused;(e=e.filter(function(n){return n.incognito&&"normal"===n.type})).length?t.An.query({
windowId:d.preferLastWnd(e).id,active:true},function(n){var e=n[0];t.tabsCreate({url:s,windowId:e.windowId,
active:false!==r.z.active,index:d.newTabIndex(e,r.z.position,false,false)},c.getRunNextCmdBy(3)),l&&t.selectWnd(e),
t.An.remove(o)}):(u="normal"===i.type?i.state:"",(f=null!=a.url)?r.e.Ge&&(l=true,u=""):a.tabId=o,a.focused=l,
t.makeWindow(a,u,function(e){f||e&&p(),f&&e?c.getRunNextCmdBy(0)(e.tabs&&e.tabs[0]||null):n(!!e)}),f&&t.An.remove(o))})
},s=!!r.z.incognito
;s&&(r.U?r.U.s.Sn:2===r.ne)?(f.showHUD(l.Xn(u)),n(0)):(i||1!==m(r.T)&&!s?t.jn(t.getCurWnd,true):t.jn(t.getCurWnd,false).then(function(n){
return n&&t.jn(t.An.query,{windowId:n.id,active:true}).then(function(e){return n.tabs=e,e&&e.length?n:void 0})
})).then(function(e){e?(s?a:o)(e):n(0)})},e.moveTabToNewWindow=I,g=function(n,u){var i=n[0];t.$n.getAll(function(n){
var o,f,l,c,a,d,s,w=false===r.z.minimized||false===r.z.min,I=false!==r.z.focused,g=n.filter(function(n){
return n.incognito===i.incognito&&"normal"===n.type&&(!w||"minimized"!==n.state)});if(g.length>0){
if(o=g.map(function(n){return n.id}),f=o.indexOf(i.windowId),o.length>=2||f<0)return l=r.z.filter,c=!!(r.z.tabs||l),
a=o.indexOf(r.le),d=r.z.last&&a>=0?a:f>=0?f+1:0,void t.An.query({
windowId:o[s=((s=(s=((s=c?d:r.T>0?d+r.T-1:d+r.T)%o.length+o.length)%o.length)!==f?s:s+(r.T>0?1:-1))%o.length+o.length)%o.length],
active:true},function(n){var e=n[0],o=b(e),a=null==o||o>e.index,d=null,s=false,w=null,g=function(){
false!==s?(I||s&&t.selectTab(s.id),t.An.move(i.id,{index:null!=o?o:-1,windowId:e.windowId},function(n){var e,o
;if(t.Mn())return u(0),t.selectWnd(i),t.Mn();if(d)for(e=d.findIndex(function(e){return e.id===n.id}),
o=0;o<d.length;o++)o!==e&&t.An.move(d[o].id,{index:n.index+o,windowId:n.windowId},t.Mn);r.U&&r.U.s.Hn===n.id&&p()}),
I&&t.selectWnd(e),false!==r.z.active&&t.selectTab(i.id,t.zn(u)),I&&s&&t.selectTab(s.id)):v.ku(i,!a,w).then(function(n){
s=n,g()})};c&&(f>=0||r.we>=52)&&(l||1!==m(r.T))?v.xu(true,0,function(n,e){if(w=n.slice(),i=n[e[1]],n=n.slice(e[0],e[2]),
r.we<52&&(n=n.filter(function(n){return n.incognito===i.incognito})),l){if(!(n=v.mu(i,n,l)).length)return void u(0)
;i=n.includes(i)?i:n[0]}s=(1!==(d=n).length||!d[0].active)&&null,g()},[],u):f>=0||r.we>=52?g():(s=null,
t.makeTempWindow_r(i.id,i.incognito,g))})}else g=n.filter(function(n){return n.id===i.windowId})
;m(r.T)>1?e.moveTabToNewWindow(u):v.ku(i,false).then(function(n){I||n&&t.selectTab(n.id),t.makeWindow({tabId:i.id,
incognito:i.incognito,focused:I},1===g.length&&"normal"===g[0].type?g[0].state:"",function(e){e&&(p(),
I&&n&&t.selectTab(n.id)),u(!!e)})})})},e.moveTabToNextWindow=g,_=function(n,u,i,o){var f,l,a,d=u[0],s=u[1],w=u[2],p={
bypassCache:true===(r.z.hard||r.z.bypassCache)},b=t.An.reload,I=n
;if(m(r.T)<2||r.z.single)b(n[o?s:d].id,p,c.getRunNextCmdBy(0));else{if(f=n[s],l=r.z.filter,n=n.slice(d,w),l){
if(!(n=v.mu(f,n,l)).length)return void i(0);f=n.includes(f)?f:n[0]}
if(n.length>20&&c.vu())c.cu("reloadTab",n.length).then(e.reloadTab.bind(null,I,[d,s,w],i));else for(a of(b(f.id,p,c.getRunNextCmdBy(0)),
n))a!==f&&b(a.id,p)}},e.reloadTab=_,x=function(n,u,i){
var o,f,l,a,d,s,p,b,I,g,_,x,y,k,j,M,O=r.z.highlighted,P=r.z.goto||(r.z.left?"left":""),A=(P+"").split(/[\/,;\s]/),N=A.length>1?A[m(r.T)>1?1:0]:P+"",R="near"===N||"reverse"===N||N.startsWith("back"),W=N.startsWith("forw"),$=R?r.T>0:W?r.T<0:"left"===N,z=R?r.T<0:W?r.T>0:"right"===N,C=N.includes("previous"),H=C&&N.includes("only")
;if(u){if(!i||!i.length)return n(0),t.Mn();if(f=i.length,l=t.selectIndexFrom(i),a=i[l],d=1,s=l,p=l+1,m(r.T)>1&&f>1){
if(b=0,i[0].pinned!==a.pinned&&!(r.T<0&&i[l-1].pinned))for(;i[b].pinned;)b++
;if((d=(I=v.getTabRange(l,f-b,f))[1]-I[0])>20&&c.vu()&&u<3)return void c.cu("removeTab",d).then(e.removeTab.bind(null,n,2,i))
;d>1&&(s=b+I[0],p=b+I[1])}else if(O){if(_="no-current"===O,(d=(g=i.filter(function(n){return n.highlighted&&n!==a
})).length+1)>1&&(_||d<f)&&t.An.remove(g.map(function(n){return n.id}),t.Mn),_)return void n(d>1)
}else if(r.z.filter&&0===v.mu(a,[a],r.z.filter).length)return void n(0)
;if(!s&&d>=f&&true!==(null!=r.z.mayClose?r.z.mayClose:r.z.allow_close))u<2?t.getCurTabs(e.removeTab.bind(null,n,3)):t.$n.getAll(T.bind(null,n,a,i));else if(u<2&&(H?(y=v.wu())>=0&&(x=t.jn(t.tabsGet,y)):(z||$&&s>0)&&(x=t.jn(t.An.query,{
windowId:a.windowId,index:$?s-1:s+1}).then(function(n){return n&&n[0]})),x))x.then(function(r){
r&&r.windowId===a.windowId&&t.qn(r)?(t.selectTab(r.id),
t.An.remove(a.id,t.zn(n))):t.getCurTabs(e.removeTab.bind(null,n,3))});else{if(k=f,
d>=f);else if(C)k=(j=!H&&p<f&&!r.ee.has(i[p].id)?i[p]:i.filter(function(n,e){return(e<s||e>=p)&&r.ee.has(n.id)
}).sort(w._t.Lr)[0])?i.indexOf(j):f;else if($||z){
for(M=k=$?s>0?s-1:p:p<f?p:s-1;M>=0&&M<f&&(M<s||M>=p)&&!t.qn(i[M]);)M+=M<s?-1:1;k=M>=0&&M<f?M:k}
k>=0&&k<f&&t.selectTab(i[k].id),h(a,i,s,p,n)}
}else((o=m(r.T)>1||O||C&&!H)?t.getCurTabs:t.getCurTab)(e.removeTab.bind(null,n,o?2:1))},e.removeTab=x,
T=function(n,e,u,i){var o,f,l=false;i=i.filter(function(n){return"normal"===n.type}),
"always"===r.z.keepWindow?l=!i.length||i.some(function(n){return n.id===e.windowId}):i.length<=1?(l=true,
(f=i[0])&&(f.id!==e.windowId?l=false:f.incognito&&!t.xn(r.newTabUrl_f)&&(o=f.id))):e.incognito||1===(i=i.filter(function(n){
return!n.incognito})).length&&i[0].id===e.windowId&&(o=i[0].id,l=true),l&&t.tabsCreate({index:u.length,url:"",windowId:o
},c.getRunNextCmdBy(3)),h(e,u,0,u.length,l?null:n)},h=function(n,e,u,i,o){var f,l,c,a=Math.max(0,e.indexOf(n))
;t.An.remove(n.id,o?t.zn(o):t.Mn),l=e.slice(a+1,i),c=e.slice(u,a),r.T<0&&(l=(f=[c,l])[0],c=f[1]),
l.length>0&&t.An.remove(l.map(function(n){return n.id}),t.Mn),c.length>0&&t.An.remove(c.map(function(n){return n.id
}),t.Mn)},e.toggleMuteTab=function(n){var e,u,i,o;e=r.z.filter,r.z.all||e||r.z.other||r.z.others?(i=function(i){
var o,c,a,d=r.z.other||r.z.others?r.U?r.U.s.Hn:r.oe:-1,s=0===i.length||-1!==d&&1===i.length&&i[0].id===d
;if(null!=r.z.mute)s=!!r.z.mute;else for(o of i)if(o.id!==d&&!t.isTabMuted(o)){s=true;break}
if(!e||(i=v.mu(u,i,e)).length){for(o of(c={muted:s},i))o.id!==d&&s!==t.isTabMuted(o)&&t.tabsUpdate(o.id,c)
;a=-1===d?"All":"Other",Promise.resolve(l.Xn(a)).then(function(e){f.showHUD(l.Xn(s?"mute":"unmute",[e||a])),n(1)})
}else n(0)},(o=v.getNecessaryCurTabInfo(e))?o.then(function(n){u=n,t.An.query({audible:true},i)}):t.An.query({
audible:true},i)):t.getCurTab(function(e){var u=e[0],i=!t.isTabMuted(u),o=null!=r.z.mute?!!r.z.mute:i
;o===i&&t.tabsUpdate(u.id,{muted:o}),f.showHUD(l.Xn(o?"muted":"unmuted")),n(1)})},e.togglePinTab=function(n,e,u){
var i,o,f,l,a,d,s,w,p=r.z.filter,b=e[1],I=n[b];if(n=p?v.mu(I,n,p):n,i=!p||n.includes(I)?!I.pinned:!!n.find(function(n){
return!n.pinned}),o={pinned:i},f=i?0:1,l=0,m(r.T)>1&&i)for(;n[l].pinned;)l++
;for(d=l+(a=v.getTabRange(b-l,n.length-l,n.length))[f]-f,
s=l+a[1-f]-f,w=[];d!==s;d+=i?1:-1)(i||n[d].pinned)&&w.push(n[d])
;(s=w.length)?(s<=30||!c.vu()?Promise.resolve(false):c.cu("togglePinTab",s)).then(function(n){n&&(w.length=1)
}).then(function(){var n,e=w.includes(I)?I.id:w[0].id;for(n of w)t.tabsUpdate(n.id,o,n.id===e?t.zn(u):t.Mn)}):u(0)},
e.toggleTabUrl=function(n,e){var a,s=t.getTabUrl(n[0]),v=r.z.reader,w=r.z.keyword
;if(s.startsWith(r.e.xe))return f.complainLimits(l.Xn(v?"noReader":"openExtSrc")),void e(0);v&&w?(a=o.fu({u:s
}))&&a.k===w?(c.overrideCmdOptions({keyword:""}),d.openUrlWithActions(a.u,0,true,n)):(s=i.er(a&&r.z.parsed?a.u:s,w),
d.openUrlWithActions(s,9,true,n)):v?r.be&&u.wn.test(s)?(s=s.startsWith("read:")?u.on(s.slice(s.indexOf("?url=")+5)):"read://".concat(new URL(s).origin.replace(/:\/\/|:/g,"_"),"/?url=").concat(u.nn(s)),
d.openUrlWithActions(s,9,true,n)):(f.complainLimits(l.Xn("noReader")),
e(0)):(s=s.startsWith("view-source:")?s.slice(12):"view-source:"+s,d.openUrlWithActions(s,9,true,n))},
e.Ft=function(n,e,u,i){var o,f,l,a,d,s,v=n.id,w=1===e;if(e&&t.Tn()&&(false!==i||null==t.getGroupId(n)))return o=0,f=-1,
l=function(){var n=t.Mn();if(n)return t.Tn().restore(null,c.getRunNextCmdBy(0)),f>=0&&t.An.remove(f),f=0,n
;(o+=1)>=5||setTimeout(function(){t.tabsGet(v,l)},50*o*o)},w&&t.tabsCreate({url:"about:blank",active:false,
windowId:n.windowId},function(n){f?f=n.id:t.An.remove(n.id)}),void t.An.remove(v,function(){return t.tabsGet(v,l),t.Mn()
});d=t.isTabMuted(n),a=function(n){d!==t.isTabMuted(n)&&t.tabsUpdate(n.id,{muted:d})},s={windowId:n.windowId,
index:n.index,url:t.getTabUrl(n),active:n.active,pinned:n.pinned,openerTabId:n.openerTabId},u&&(s=Object.assign(u,s)),
null!=s.index&&s.index++,t.openMultiTabs(s,1,true,[null],i,n,function(n){n&&a&&a(n),
n?c.runNextOnTabLoaded(r.z,n):c.runNextCmd(0)}),t.An.remove(v)}});