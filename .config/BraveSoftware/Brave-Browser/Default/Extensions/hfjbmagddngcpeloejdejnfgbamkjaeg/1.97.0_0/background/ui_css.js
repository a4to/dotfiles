"use strict"
;__filename="background/ui_css.js",define(["require","exports","./store","./utils","./settings","./ports"],function(n,i,r,t,e,o){
var u,s,c,f,a,l;Object.defineProperty(i,"__esModule",{value:true}),i.ni=i.ii=i.mergeCSS=i.ri=void 0,c=function(n,o){
return-1===n?i.mergeCSS(o,-1):(2===n&&(r.Y=null),(c=0===n&&r.de.get("findCSS"))?(s=null,r.A=a(c),r.q=o.slice(u.length),
void(r.pe.c=r.de.get("omniCSS")||"")):void t.a("vimium-c.css").then(function(t){var o,s,c,a,l;u.slice(u.indexOf(",")+1),
r.we<54&&(t=t.replace(/user-select\b/g,"-webkit-$&")),r.we<62&&(t=t.replace(/#[\da-f]{4}([\da-f]{4})?\b/gi,function(n){
n=5===n.length?"#"+n[1]+n[1]+n[2]+n[2]+n[3]+n[3]+n[4]+n[4]:n
;var i=parseInt(n.slice(1),16),r=i>>16&255,t=i>>8&255,e=(255&i)/255+""
;return"rgba(".concat(i>>>24,",").concat(r,",").concat(t,",").concat(e.slice(0,4),")")})),
s=(t=(o=f(t)).ui).indexOf("all:"),c=t.lastIndexOf("{",s),a=r.we>=53?t.indexOf(";",s):t.length,
t=t.slice(0,c+1)+t.slice(s,a+1)+t.slice(t.indexOf("\n",a)+1||t.length),r.we>64&&true?(s=t.indexOf("display:"),
c=t.lastIndexOf("{",s),t=t.slice(0,c+1)+t.slice(s)):t=t.replace("contents","block"),
r.we<73&&(t=t.replace("3px 5px","3px 7px")),r.we<69&&(t=t.replace(".LH{",".LH{box-sizing:border-box;")),
r.be&&r.we<89&&(t=t.replace("forced-colors","-ms-high-contrast")),t=t.replace(/\n/g,""),
r.we<85&&(t=t.replace(/0\.01|\/\*!DPI\*\/ ?[\d.]+/g,"/*!DPI*/"+(r.we<48?1:.5))),e.io("innerCSS",u+t),
e.io("findCSS",(l=o.find).length+"\n"+l),i.mergeCSS(r.ge.userDefinedCss,n)}));var c},i.ri=c,f=function(n){
var i,r,t=n?n.split(/^\/\*\s?#!?([A-Za-z:]+)\s?\*\//m):[""],e={ui:t[0].trim()}
;for(i=1;i<t.length;i+=2)e[r=t[i].toLowerCase()]=(e[r]||"")+t[i+1].trim();return e},a=function(n){
var i=(n=n.slice(n.indexOf("\n")+1)).indexOf("\n")+1,r=n.indexOf("\n",i);return{c:n.slice(0,i-1).replace("  ","\n"),
s:n.slice(i,r).replace("  ","\n"),i:n.slice(r+1)}},l=function(n,t){
var s,c,l,d,S,v,b,g,m,p,C,x=r.de.get("innerCSS"),_=x.indexOf("\n");if(x=_>0?x.slice(0,_):x,c=(s=f(n)).ui?x+"\n"+s.ui:x,
l=s["find:host"],d=s["find:selection"],S=s.find,v=s.omni,b="omniCSS",_=(x=r.de.get("findCSS")).indexOf("\n"),
g=(x=x.slice(0,_+1+ +x.slice(0,_))).indexOf("\n",_+1),m=x.slice(0,g).indexOf("  "),d=d?"  "+d.replace(/\n/g," "):"",
(m>0?x.slice(m,g)!==d:d)&&(x=x.slice(_+1,m=m>0?m:g)+d+x.slice(g),g=m-(_+1)+d.length,_=-1),p=x.indexOf("\n",g+1),
C=x.slice(0,p).indexOf("  ",g),
l=l?"  "+l.replace(/\n/g," "):"",(C>0?x.slice(C,p)!==l:l)&&(x=x.slice(_+1,C>0?C:p)+l+x.slice(p),_=-1),
_<0&&(x=x.length+"\n"+x),S=S?x+"\n"+S:x,x=(r.de.get(b)||"").split("\n",1)[0],v=v?x+"\n"+v:x,-1===t)return{
ui:c.slice(u.length),find:a(S),omni:v};e.io("innerCSS",c),e.io("findCSS",S),e.io(b,v||null),i.ri(0,c),
0!==t&&1!==t&&(o.po(function(n){var t;for(t of n.wo)8&t.s.Kn&&t.postMessage({N:12,H:r.q,f:32&t.s.Kn?i.ni(t.s):void 0})
}),e.no({N:47,d:{c:r.pe.c}}))},i.mergeCSS=l,i.ii=function(n,i){var e,o,u,s,c,f,a=r.pe.s
;!n.o&&r.E||(o=" ".concat(n.t," "),
s=(u=a&&" ".concat(a," ")).includes(o),e=(e=(c=null!=n.e?n.e:s)?s?a:a+o:u.replace(o," ")).trim().replace(t.hn," "),
false!==n.b?(n.o&&(r.E=c!==" ".concat(r.ge.vomnibarOptions.styles," ").includes(o)),e!==a&&(r.pe.s=e,f={N:47,d:{s:e}},
t.c(r.ue.slice(0),function(n){return n!==i&&r.ue.includes(n)&&n.postMessage(f),1}))):r.pe.s=e)},i.ni=function(n){
var i=r.A;return r.we<86&&n.or.startsWith("file://")?s||(s={
c:i.c+"\n.icon.file { -webkit-user-select: auto !important; user-select: auto !important; }",s:i.s,i:i.i}):i},
e.lo.then(function(){u=r.e.Oe+","+r.we+";",r.q=r.de.get("innerCSS")||"",r.q&&!r.q.startsWith(u)?(r.vomnibarPage_f="",
i.ri(1,r.q)):i.ri(0,r.q),r.ce.userDefinedCss=i.mergeCSS})});