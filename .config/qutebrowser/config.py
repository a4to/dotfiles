#!/usr/bin/env python

######################################################################################################################
#                                          QuteBrowser Configuration File :                                          #
######################################################################################################################

## General

c.auto_save.session = False
c.tabs.show = 'never'
c.statusbar.show = 'never'

config.set("colors.webpage.darkmode.enabled", True)

config.bind('Z', 'hint links spawn mpv {hint-url}')
config.bind('<Ctrl-z>', 'hint links spawn st -e yt-dlp {hint-url}')
config.bind('t', 'open -t')
config.bind('T', 'set-cmd-text -s :open -t')
config.bind('ct', 'config-cycle tabs.show always never')
config.bind('cb', 'config-cycle statusbar.show always never')
config.bind('cc', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')
config.bind('z', 'back')
config.bind('x', 'forward')
config.bind('w', 'tab-close')
config.bind('s', 'tab-next')
config.bind('a', 'tab-prev')
config.bind('Q', 'tab-clone')
config.bind('q', 'hint links open -t {hint-url}')

## Site Bindings ##

config.bind('<Ctrl-y>', 'open https://www.youtube.com/')
config.bind('<Ctrl-b>', 'open https://vault.bitwarden.com/#/vault')
config.bind('<Ctrl-v>', 'open https://my.vultr.com/')
config.bind('<Ctrl-g>', 'open https://www.github.com/')
config.bind('<Ctrl-m>', 'open https://configure.zsa.io/moonlander/layouts/BzNLA/latest/0')

# config.bind('<Ctrl-c>', 'config-cycle colors.webpage.darkmode.enabled True False ;; config-cycle colors.webpage.darkmode.enabled True False')
# config.bind('<Ctrl-c>', 'config-set colors.webpage.darkmode.enabled False')
# config.bind('<Ctrl-c>', "config-cycle colors.webpage.darkmode.enabled", True, False)


######################################################################################################################
#                                                       Hints :                                                      #
######################################################################################################################

# To auto-generate this file from qutebrowser, run: config-write-py
# To update AddBlock, run : addblock-update






######################################################################################################################
#                                            Auto-Generated Configurations :                                         #
######################################################################################################################

config.load_autoconfig(False)
c.auto_save.session = False
config.set('content.cookies.accept', 'all', 'chrome-devtools://*')
config.set('content.cookies.accept', 'all', 'devtools://*')
config.set('content.headers.accept_language', '', 'https://matchmaker.krunker.io/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}', 'https://web.whatsapp.com/')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}; rv:90.0) Gecko/20100101 Firefox/90.0', 'https://accounts.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36', 'https://*.slack.com/*')
config.set('content.images', True, 'chrome-devtools://*')
config.set('content.images', True, 'devtools://*')
config.set('content.javascript.enabled', True, 'chrome-devtools://*')
config.set('content.javascript.enabled', True, 'devtools://*')
config.set('content.javascript.enabled', False, 'chrome://*/*')
config.set('content.javascript.enabled', True, 'qute://*/*')

######################################################################################################################
######################################################################################################################
