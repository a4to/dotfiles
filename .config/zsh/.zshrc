#########################################################################################################################################################################################
#########################################################################################################################################################################################
###                                                                                _______| |__                                                                                       ###
##                                                                                |_  / __| '_ \                                                                                       ##
###                                                                                / /\__ \ | | |                                                                                     ###
##                                                                                /___|___/_| |_|                                                                                      ##
###                                                                                                                                                                                   ###
#########################################################################################################################################################################################
##########################################################################   --->  Z-SHELL CONFIG  <---   ###############################################################################
#########################################################################################################################################################################################

                                                                   ## ###  DO NOT REMOVE THE FOLLOWING LINES  ### ##

                                                                         source "$HOME"/.config/shell/colrc
                                                                        source "$HOME"/.config/zsh/zcore.zsh
                                                                       source "$HOME"/.config/zsh/zprompt.zsh
                                                                     source /usr/share/zsh/plugins/betterzsh.zsh
                                                                       source "$HOME"/.config/shell/functionrc
                                                                       source "$HOME"/.config/shell/wallpaperrc
                                                                        source "$HOME"/.zprofile >/dev/null 2>&1

                                                                                      Nu1LMap5

 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###
 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###
                                                                       

                                                                     ## ###  UNCOMMENT TO ENABLE FEATURES  ### ##



                                                                       #---------------------------------------#
                                                                      # #  --> SHELL PROMPTS AND HEADERS <--  # #
                                                                       #---------------------------------------#


#                                                              Below are listed the available shell prompts and headers,
#                                                            It is recommended to run the alias commands referenced below
#                                                           in opposed to changing or uncommenting the following lines below.
                                                            
#                                                            This will end up keeping your zshrc much neater, as well as 
#                                                                             assist in avoiding errors.




                       #-------------------------#
                      # # --> SHELL HEADERS <-- # # 
                       #-------------------------#

##                            Active Header : 
                         
                                colHeader 





##     Available shell prompts :

# barHeader      #  ( alias --> bar )
# colHeader      #  ( alias --> bar )
# uniHeader      #  ( alias --> bar )



                       #-------------------------#
                      # # --> SHELL PROMPTS <-- # #
                       #-------------------------#

##                            Active Prompt : 
                              
                        eval $(starship init zsh)




##     Available shell prompts :

# prompt xxx      ( alias --> xxx  )
# prompt soul     ( alias --> soul )
# prompt nu1l     ( alias --> nu1l )
# prompt luke     ( alias --> luke )
# prompt void     ( alias --> void )

# star            ( alias --> star )

##   NOTE: The starship prompt must be 
##         installed in prder for the star or 
##         (starship) prompt to take effect.



 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###

                                                                              ## ##    SCRIPTS    ## ##

 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###

## Change directories automatically when exiting lf,
## as well as bind 'Ctrl + o' to run lfcd.

lfcd(){ \
  tmp="$(mktemp)" ; lf -last-dir-path="$tmp" "$@"
  [ -f "$tmp" ] && dir="$(cat "$tmp")" && 
    rm -f "$tmp" >/dev/null 2>&1
  [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && 
    cd "$dir" ; }


blue(){ echo -e "$(tput bold; tput setaf 6)${*}$(tput sgr0)"; }
green(){ echo -e "$(tput bold; tput setaf 2)${*}$(tput sgr0)"; }
yellow(){ echo -e "$(tput bold; tput setaf 3)${*}$(tput sgr0)"; }
red(){ echo -e "$(tput bold; tput setaf 1)${*}$(tput sgr0)"; }
white(){ echo -e "$(tput bold; tput setaf 7)${*}$(tput sgr0)"; }
pink(){ echo -e "$(tput bold; tput setaf 5)${*}$(tput sgr0)"; }
bluen(){ echo -en "$(tput bold; tput setaf 6)${*}$(tput sgr0)"; }
greenn(){ echo -en "$(tput bold; tput setaf 2)${*}$(tput sgr0)"; }
yellown(){ echo -en "$(tput bold; tput setaf 3)${*}$(tput sgr0)"; }
redn(){ echo -en "$(tput bold; tput setaf 1)${*}$(tput sgr0)"; }
whiten(){ echo -en "$(tput bold; tput setaf 7)${*}$(tput sgr0)"; }
pinkn(){ echo -en "$(tput bold; tput setaf 5)${*}$(tput sgr0)"; }


# /usr/bin/wal -f $HOME/.local/share/Nu1LL1nuX/colorSchemes/74.col>/dev/null 2>&1

## Main Key Bindings :

bindkey -s '^o' 'lfcd\n'
bindkey -s '^x' 'lfcd\n'
bindkey -s '^p' 'mkmci\n'
bindkey -s '^v' 'pulsemixer\n'
bindkey -s "^z" 'cls\n'
bindkey -s '^_' 'rwp\n' 
bindkey -s "^L" 'clear\n'
bindkey -s "^k" 'zrc\n'
bindkey -s "^n" 'vV\n'
bindkey -s "^s" 'ccrc\n'
bindkey -s "^b" 'sudo\ blueberry\n'
bindkey -s "^f" 'fzf\n'
bindkey -s "^a" 'arc\n'
bindkey -s "^\\" 'v\ $(mktemp)\n'
bindkey -s '<a-w>' 'qt\n'
bindkey -s '^g' 'gacp\n'
bindkey -s '^u' 'wp34\n'
bindkey -s '^Y' 'pkgup\n'
bindkey -s '^x' 'nvim\ $HOME/.config/shell/functionrc\n'
bindkey -s '^u' 'mkbuild\n'
bindkey -s '^r' 'ga\ .;comu;pushom;pushsm\n'

ezlf() {
	EZLF_TMPDIR="$(mktemp -d -t EZLF-XXXXXX)"
	export EZLF_TMP
	./lf -last-dir-path "$EZLF_TMP/lastdir" \
		-command "source /usr/share/EZ/ezlfrc" "$@"
  [ -f "$tmp" ] && dir="$(cat "$tmp")" && 
  [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && 
    cd "$dir" ; rm -rf "$EZLF_TMP"
	unset EZLF_TMPDIR ; }

 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###
 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###
