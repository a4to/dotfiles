# denops-helloworld.vim

Example plugins of [denops.vim](https://github.com/vim-denops/denops.vim).

Use your favorite Vim plugin manager to install it.

## Plugins

### [example-minimal](./denops/example-minimal)

A minimal denops plugin which add `DenopsHello` command that echo "Hello".

### [example-standard](./denops/example-standard)

A standard denops plugin for invoking [ripgrep][ripgrep] internally. It adds

- `DenopsGrep` command to grep
- `<Plug>(denops-grep)` (default to `<Leader>gp`) to grep

[ripgrep]: https://github.com/BurntSushi/ripgrep

## License

Codes in this repository are licensed under [CC0](./LICENSE).

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/deed.ja)
